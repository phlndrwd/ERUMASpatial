/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainInterface.java
 *
 * <p>This class is the main interface from which all program functionality can
 * be accessed. It is also the place that displays real-time output from the
 * model run and the place where the model setup can be configured by accessing
 * the variable dialog interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui; // Part of the user interface package

import model.MainThread;
import param.Parameters;
import java.awt.Graphics; // Uses graphics
import java.awt.event.WindowAdapter; // Accesses window events
import java.awt.event.WindowEvent; // Handles window events
import java.awt.image.BufferedImage; // Uses offscreen images
import javax.swing.JFrame; // Extends this class
import javax.swing.JOptionPane;
import param.Options;
import tools.Tools;

@SuppressWarnings( "serial" )
public final class MainInterface extends JFrame {

    // Buffered image passed to the CA panels
    private final BufferedImage mRightOffscreenImage;
    private final BufferedImage mLeftOffscreenImage;
    // Graphics componenta of the CA panel images
    public Graphics mRightImageGraphics;
    public Graphics mLeftImageGraphics;

    private final ParametersDialog mParametersDialog;
    private final OptionsDialog mOptionsDialog;
    private final AboutDialog mAboutFrame;

    private boolean mIsThreadRunning; // Thread stop and start control flag
    private Thread mThread; // The thread

    /*
     * Class constructor, intializes class objects and sets up the interface.
     */
    public MainInterface() {

        mRightOffscreenImage = new BufferedImage( 402, 402, BufferedImage.TYPE_INT_RGB );
        mLeftOffscreenImage = new BufferedImage( 402, 402, BufferedImage.TYPE_INT_RGB );

        mRightImageGraphics = mRightOffscreenImage.getGraphics();
        mLeftImageGraphics = mLeftOffscreenImage.getGraphics();

        mAboutFrame = new AboutDialog( this, true );
        mParametersDialog = new ParametersDialog( this, true );
        mOptionsDialog = new OptionsDialog( this, true );

        initComponents();
        stopThread();
        setLocationRelativeTo( null );    // Centre on screen
        mProgressBar.setStringPainted( true );
        mProgressBar.setString( "t = 0" );

        // Add window listener so closing the window explcitly stops the thread
        addWindowListener( new WindowAdapter() {

            @Override
            public void windowClosing( WindowEvent e ) {
                stopThread();
            }
        } );
    }

    /*
     * A generic method to handle the relevant calls to the PanelLegend objects
     * in order to draw them according to the options selected by the user from
     * the variables dialog
     * @param panelLegend The PanelLegend to be updated
     * @param colour The colour to paint it
     * @param title The data variable name it represents
     */
    private void updateLegendPanels( LegendPanel panelLegend, String colour, String title ) {

        panelLegend.setColour( colour );
        panelLegend.setTitle( title );

        if( title.equalsIgnoreCase( "Temperature" ) || title.equalsIgnoreCase( "Adaptation" ) ) {
            if( Parameters.isFixedTemperature() ) {
                panelLegend.setMinimumValue( Parameters.getTemperatureMinimum() );
                panelLegend.setMaximumValue( Parameters.getTemperatureMaximum() );
            } else {
                panelLegend.setMinimumValue( Parameters.getTemperatureMinimum() );
                panelLegend.setMaximumValue( Parameters.getTemperatureMaximum() + Parameters.getTemperatureVariation() );
            }
        } else if( title.equalsIgnoreCase( "Effect" ) ) {
            panelLegend.setMinimumValue( -1 );
            panelLegend.setMaximumValue( 1 );
        }
        if( title.equalsIgnoreCase( "Population Size" ) ) {
            panelLegend.setMinimumValue( 0 );
            panelLegend.setMaximumValue( Parameters.getCarryingCapacity() );
        } else if( title.equalsIgnoreCase( "Fitness" ) ) {
            panelLegend.setMinimumValue( 0 );
            panelLegend.setMaximumValue( 1 );
        }
        panelLegend.repaint();
    }

    /*
     * Updates the interface appropriately, creates and starts the main thread
     */
    private void startThread() {

        if( Options.getGridSize() == 1 && Parameters.isFixedTemperature() == false ) {
            JOptionPane.showMessageDialog( this, "Defaulting to fixed temperature for a single population.", "Information", JOptionPane.INFORMATION_MESSAGE );
            Parameters.setFixedTemperature( true );
        }

        updateLegendPanels( ( LegendPanel )mPanelRightLegend, Options.getRightGridColour(), Options.getRightGridData() );
        updateLegendPanels( ( LegendPanel )mPanelLeftLegend, Options.getLeftGridColour(), Options.getLeftGridData() );
        mMenuStart.setEnabled( false ); // Disable start menu option
        mMenuStop.setEnabled( true );        // Enable stop button
        mMenuEdit.setEnabled( false );    // Disable options button
        mMenuHelp.setEnabled( false );
        mIsThreadRunning = true;                   // CA is running
        mProgressBar.setMaximum( Parameters.getTimeMaximum() );
        /*
         * Instantiate a new thread requiring a runnable object at a parameter,
         * create a new instance of MainThread with a reference to this class as
         * the parent so that calls can be made back to methods and components
         * of it.
         */
        mThread = new Thread( new MainThread( this ) );
        mThread.start();

    }

    /*
     * Updates the interface appropriately and sets the running flag so that
     * the thread will see and terminate its main loop.
     */
    public void stopThread() {

        mMenuStart.setEnabled( true );
        mMenuStop.setEnabled( false );
        mMenuEdit.setEnabled( true );
        mMenuHelp.setEnabled( true );
        mIsThreadRunning = false;
    }

    /**
     * This method is called from within the constructor to initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mPanelLeftGrid = new ui.GridPanel(mLeftOffscreenImage);
        mPanelRightGrid = new ui.GridPanel(mRightOffscreenImage);
        mPanelLeftLegend = new ui.LegendPanel();
        mPanelRightLegend = new ui.LegendPanel();
        mProgressBar = new javax.swing.JProgressBar();
        mMenuMain = new javax.swing.JMenuBar();
        mMenuFile = new javax.swing.JMenu();
        mMenuExit = new javax.swing.JMenuItem();
        mMenuEdit = new javax.swing.JMenu();
        mMenuVariables = new javax.swing.JMenuItem();
        mMenuOptions = new javax.swing.JMenuItem();
        mMenuRun = new javax.swing.JMenu();
        mMenuStart = new javax.swing.JMenuItem();
        mMenuStop = new javax.swing.JMenuItem();
        mMenuHelp = new javax.swing.JMenu();
        mMenuAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ERUMASpatial");
        setResizable(false);

        mPanelLeftGrid.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mPanelLeftGrid.setPreferredSize(new java.awt.Dimension(402, 402));

        javax.swing.GroupLayout mPanelLeftGridLayout = new javax.swing.GroupLayout(mPanelLeftGrid);
        mPanelLeftGrid.setLayout(mPanelLeftGridLayout);
        mPanelLeftGridLayout.setHorizontalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        mPanelLeftGridLayout.setVerticalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        mPanelRightGrid.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mPanelRightGrid.setPreferredSize(new java.awt.Dimension(402, 402));

        javax.swing.GroupLayout mPanelRightGridLayout = new javax.swing.GroupLayout(mPanelRightGrid);
        mPanelRightGrid.setLayout(mPanelRightGridLayout);
        mPanelRightGridLayout.setHorizontalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        mPanelRightGridLayout.setVerticalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        mPanelLeftLegend.setPreferredSize(new java.awt.Dimension(402, 50));

        javax.swing.GroupLayout mPanelLeftLegendLayout = new javax.swing.GroupLayout(mPanelLeftLegend);
        mPanelLeftLegend.setLayout(mPanelLeftLegendLayout);
        mPanelLeftLegendLayout.setHorizontalGroup(
            mPanelLeftLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 402, Short.MAX_VALUE)
        );
        mPanelLeftLegendLayout.setVerticalGroup(
            mPanelLeftLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        mPanelRightLegend.setPreferredSize(new java.awt.Dimension(402, 50));

        javax.swing.GroupLayout mPanelRightLegendLayout = new javax.swing.GroupLayout(mPanelRightLegend);
        mPanelRightLegend.setLayout(mPanelRightLegendLayout);
        mPanelRightLegendLayout.setHorizontalGroup(
            mPanelRightLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 402, Short.MAX_VALUE)
        );
        mPanelRightLegendLayout.setVerticalGroup(
            mPanelRightLegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        mMenuFile.setText("File");

        mMenuExit.setText("Exit");
        mMenuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuExitActionPerformed(evt);
            }
        });
        mMenuFile.add(mMenuExit);

        mMenuMain.add(mMenuFile);

        mMenuEdit.setText("Edit");

        mMenuVariables.setText("Parameters");
        mMenuVariables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuVariablesActionPerformed(evt);
            }
        });
        mMenuEdit.add(mMenuVariables);

        mMenuOptions.setText("Options");
        mMenuOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuOptionsActionPerformed(evt);
            }
        });
        mMenuEdit.add(mMenuOptions);

        mMenuMain.add(mMenuEdit);

        mMenuRun.setText("Run");

        mMenuStart.setText("Start");
        mMenuStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuStartActionPerformed(evt);
            }
        });
        mMenuRun.add(mMenuStart);

        mMenuStop.setText("Stop");
        mMenuStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuStopActionPerformed(evt);
            }
        });
        mMenuRun.add(mMenuStop);

        mMenuMain.add(mMenuRun);

        mMenuHelp.setText("Help");

        mMenuAbout.setText("About");
        mMenuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuAboutActionPerformed(evt);
            }
        });
        mMenuHelp.add(mMenuAbout);

        mMenuMain.add(mMenuHelp);

        setJMenuBar(mMenuMain);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 810, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mPanelLeftLegend, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mPanelRightGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mPanelRightLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelRightGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mPanelLeftLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelRightLegend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mMenuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuAboutActionPerformed
        Tools.centreWindow( mAboutFrame );
        mAboutFrame.setVisible( true );
    }//GEN-LAST:event_mMenuAboutActionPerformed

    private void mMenuStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuStopActionPerformed
        stopThread();
    }//GEN-LAST:event_mMenuStopActionPerformed

    private void mMenuStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuStartActionPerformed
        startThread();
    }//GEN-LAST:event_mMenuStartActionPerformed

    private void mMenuVariablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuVariablesActionPerformed
        Tools.centreWindow( mParametersDialog );
        mParametersDialog.setVisible( true );
    }//GEN-LAST:event_mMenuVariablesActionPerformed

    private void mMenuExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuExitActionPerformed
        System.exit( 0 );
    }//GEN-LAST:event_mMenuExitActionPerformed

    private void mMenuOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuOptionsActionPerformed
        Tools.centreWindow( mOptionsDialog );
        mOptionsDialog.setVisible( true );
    }//GEN-LAST:event_mMenuOptionsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {

            @Override
            public void run() {
                MainInterface mainInterface = new MainInterface();
                Tools.centreWindow( mainInterface );
                mainInterface.setVisible( true );
            }
        } );
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem mMenuAbout;
    private javax.swing.JMenu mMenuEdit;
    private javax.swing.JMenuItem mMenuExit;
    private javax.swing.JMenu mMenuFile;
    private javax.swing.JMenu mMenuHelp;
    private javax.swing.JMenuBar mMenuMain;
    private javax.swing.JMenuItem mMenuOptions;
    private javax.swing.JMenu mMenuRun;
    private javax.swing.JMenuItem mMenuStart;
    private javax.swing.JMenuItem mMenuStop;
    private javax.swing.JMenuItem mMenuVariables;
    public javax.swing.JPanel mPanelLeftGrid;
    private javax.swing.JPanel mPanelLeftLegend;
    public javax.swing.JPanel mPanelRightGrid;
    private javax.swing.JPanel mPanelRightLegend;
    public javax.swing.JProgressBar mProgressBar;
    // End of variables declaration//GEN-END:variables

    /*
     * Getter for property running.
     * @return Value of property running.
     */
    public boolean isThreadRunning() {
        return mIsThreadRunning;
    }
}
