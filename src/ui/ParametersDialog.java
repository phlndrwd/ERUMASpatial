/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ParametersDialog.java
 *
 * <p>This class is the interface from which all of the model variables and
 * settings can be configured by the user and saved to the global variables
 * object used by all the classes.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui; // Part of the user interface package

import java.awt.event.WindowAdapter; // Accesses window events
import java.awt.event.WindowEvent; // Handles window events
import java.awt.Frame; // Handles reference to the parent
import javax.swing.JDialog; // Extends this class
import javax.swing.JSpinner; // Uses this component for numerical input
import javax.swing.SpinnerModel; // Holds spinner input model
import javax.swing.SpinnerNumberModel; // Type of spinner input model
import param.Parameters; // Creates the model variables object

@SuppressWarnings( "serial" )
public class ParametersDialog extends JDialog {

    /*
     * Class constructor, centres on screen, creates and stores the variables
     * for the first time.
     */
    public ParametersDialog( Frame parent, boolean modal ) {

        super( parent, modal );
        initComponents();
        save(); // Store variables

        // Add a window listener so that the close button acts as cancelling
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );
    }

    /*
     * Resets the widgets back to their stored values, for interface cancel
     * events when saving altered values is not required.
     */
    private void reset() {
        // Environmental variables
        mSpinnerTimeMaximum.setValue( Parameters.getTimeMaximum() );
        mSpinnerTemperatureMinimum.setValue( Parameters.getTemperatureMinimum() );
        mSpinnerTemperatureMaximum.setValue( Parameters.getTemperatureMaximum() );
        mCheckFixedTemperature.setSelected( !Parameters.isFixedTemperature() );
        mSpinnerTemperatureVariation.setValue( Parameters.getTemperatureVariation() );
        mSpinnerForcingStrength.setValue( Parameters.getForcingStrength() * 10E3 );
        // Population variables
        mSpinnerCarryingCapacity.setValue( Parameters.getCarryingCapacity() );
        mCheckFixedPopulation.setSelected( Parameters.isFixedPopulation() );
        mSpinnerDiffusionParameter.setValue( Parameters.getDiffusionParameter() );
        mSpinnerNicheConstructionStrength.setValue( Parameters.getNicheConstructionStrength() * 10E3 );
        mSpinnerMigrationProbability.setValue( Parameters.getMigrationProbability() * 10E3 );
        mSpinnerSeedRate.setValue( Parameters.getSeedRate() );
        // Organismic variables
        mSpinnerAdaptionMinimum.setValue( Parameters.getAdaptionMinimum() );
        mSpinnerAdaptionMaximum.setValue( Parameters.getAdaptionMaximum() );
        mCheckFixedImpact.setSelected( Parameters.isFixedImpact() );
        mSpinnerFitnessParabolaSpan.setValue( Parameters.getFitnessParabolaSpan() );
        mSpinnerDeathRate.setValue( Parameters.getDeathRate() );
        mCheckFixedGenotype.setSelected( Parameters.isFixedGenotype() );
        mSpinnerAdaptionMutationRate.setValue( Parameters.getMutationRate( 0 ) );
        mSpinnerAdaptionMuStandDev.setValue( Parameters.getMutationStandDev( 0 ) );
        mSpinnerEffectMutationRate.setValue( Parameters.getMutationRate( 1 ) );
        mSpinnerEffectMutationStandDev.setValue( Parameters.getMutationStandDev( 1 ) );

        if( Parameters.getMigrationScheme() == 1 ) {
            mRadioMigrationPopulation.setSelected( true );
        }
        if( Parameters.getMigrationScheme() == 2 ) {
            mRadioMigrationFitness.setSelected( true );
        }
        if( Parameters.getMigrationScheme() == 3 ) {
            mRadioMigrationScore.setSelected( true );
        }
        if( Parameters.getMigrationScheme() == 4 ) {
            mRadioMigrationRandom.setSelected( true );
        }
        if( Parameters.getMigrationScheme() == 5 ) {
            mRadioMigrationNone.setSelected( true );
        }
        setGenotypeWidgets();
    }

    /*
     * Saves the values configured on the interface to the variables object
     */
    private void save() {
        // Environmental variables
        Parameters.setTimeMaximum( Integer.parseInt( mSpinnerTimeMaximum.getValue().toString() ) );
        Parameters.setTemperatureMinimum( Integer.parseInt( mSpinnerTemperatureMinimum.getValue().toString() ) );
        Parameters.setTemperatureMaximum( Integer.parseInt( mSpinnerTemperatureMaximum.getValue().toString() ) );
        Parameters.setTemperatureIncrement( ( double )( Parameters.getTemperatureMaximum() - Parameters.getTemperatureMinimum() ) / Parameters.getTimeMaximum() );
        Parameters.setFixedTemperature( !mCheckFixedTemperature.isSelected() );
        Parameters.setTemperatureVariation( Integer.parseInt( mSpinnerTemperatureVariation.getValue().toString() ) );
        Parameters.setForcingStrength( Double.parseDouble( mSpinnerForcingStrength.getValue().toString() ) * 10E-5 );
        // Population variables
        Parameters.setCarryingCapacity( Integer.parseInt( mSpinnerCarryingCapacity.getValue().toString() ) );
        Parameters.setFixedPopulation( mCheckFixedPopulation.isSelected() );
        Parameters.setDiffusionParameter( Double.parseDouble( mSpinnerDiffusionParameter.getValue().toString() ) );
        Parameters.setNicheConstructionStrength( Double.parseDouble( mSpinnerNicheConstructionStrength.getValue().toString() ) * 10E-5 );
        Parameters.setMigrationProbability( Double.parseDouble( mSpinnerMigrationProbability.getValue().toString() ) * 10E-5 );
        Parameters.setSeedRate( Double.parseDouble( mSpinnerSeedRate.getValue().toString() ) );
        // Organismic variables
        Parameters.setAdaptionMinimum( Integer.parseInt( mSpinnerAdaptionMinimum.getValue().toString() ) );
        Parameters.setAdaptionMaximum( Integer.parseInt( mSpinnerAdaptionMaximum.getValue().toString() ) );
        Parameters.setFixedImpact( mCheckFixedImpact.isSelected() );
        Parameters.setFitnessParabolaSpan( Integer.parseInt( mSpinnerFitnessParabolaSpan.getValue().toString() ) );
        Parameters.setDeathRate( Double.parseDouble( mSpinnerDeathRate.getValue().toString() ) );
        Parameters.setFixedGenotype( mCheckFixedGenotype.isSelected() );
        Parameters.setMutationRate( 0, Double.parseDouble( mSpinnerAdaptionMutationRate.getValue().toString() ) );
        Parameters.setMutationRate( 1, Double.parseDouble( mSpinnerEffectMutationRate.getValue().toString() ) );
        Parameters.setMutationStandDev( 0, Double.parseDouble( mSpinnerAdaptionMuStandDev.getValue().toString() ) );
        Parameters.setMutationStandDev( 1, Double.parseDouble( mSpinnerEffectMutationStandDev.getValue().toString() ) );

        if( mRadioMigrationPopulation.isSelected() ) {
            Parameters.setMigrationScheme( 1 );
        }
        if( mRadioMigrationFitness.isSelected() ) {
            Parameters.setMigrationScheme( 2 );
        }
        if( mRadioMigrationScore.isSelected() ) {
            Parameters.setMigrationScheme( 3 );
        }
        if( mRadioMigrationRandom.isSelected() ) {
            Parameters.setMigrationScheme( 4 );
        }
        if( mRadioMigrationNone.isSelected() ) {
            Parameters.setMigrationScheme( 5 );
        }
        setGenotypeWidgets();
    }

    private void setGenotypeWidgets() {
        if( mCheckFixedGenotype.isSelected() == true ) {
            mLabelAdaptionMutationRate.setEnabled( false );
            mLabelAdaptionMuStandDev.setEnabled( false );
            mLabelEffectMutationRate.setEnabled( false );
            mLabelEffectMutationStandDev.setEnabled( false );
            mSpinnerAdaptionMutationRate.setEnabled( false );
            mSpinnerAdaptionMuStandDev.setEnabled( false );
            mSpinnerEffectMutationRate.setEnabled( false );
            mSpinnerEffectMutationStandDev.setEnabled( false );
        } else if( mCheckFixedGenotype.isSelected() == false ) {
            mLabelAdaptionMutationRate.setEnabled( true );
            mLabelAdaptionMuStandDev.setEnabled( true );
            mLabelEffectMutationRate.setEnabled( true );
            mLabelEffectMutationStandDev.setEnabled( true );
            mSpinnerAdaptionMutationRate.setEnabled( true );
            mSpinnerAdaptionMuStandDev.setEnabled( true );
            mSpinnerEffectMutationRate.setEnabled( true );
            mSpinnerEffectMutationStandDev.setEnabled( true );
        }
    }

    /*
     * Cancels alteration of variables and makes the dialog invisible
     */
    private void cancel() {
        setVisible( false );
        reset();
    }

    /*
     * Saves the variable configuration and makes the dialog invisible
     */
    private void ok() {
        setVisible( false );
        save();
    }

    /*
     * Saves the variable configuration
     */
    private void apply() {
        save();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mGroupMigration = new javax.swing.ButtonGroup();
        mButtonOK = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonApply = new javax.swing.JButton();
        mPanelOrganism = new javax.swing.JPanel();
        SpinnerModel modelGamma = new SpinnerNumberModel(0.005, 0, 0.05, 0.001);
        mSpinnerDeathRate = new JSpinner(modelGamma);
        mLabelDeathRate = new javax.swing.JLabel();
        mLabelFitnessParabolaSpan = new javax.swing.JLabel();
        SpinnerModel modelLamda = new SpinnerNumberModel(5, 0, 50, 1);
        mSpinnerFitnessParabolaSpan = new JSpinner(modelLamda);
        SpinnerModel modelAMax = new SpinnerNumberModel(85, 50, 100, 1);
        mSpinnerAdaptionMaximum = new JSpinner(modelAMax);
        mLabelAdaptionMaximum = new javax.swing.JLabel();
        SpinnerModel modelAMin = new SpinnerNumberModel(15, 0, 50, 1);
        mSpinnerAdaptionMinimum = new JSpinner(modelAMin);
        mLabelAdaptionMinimum = new javax.swing.JLabel();
        mCheckFixedImpact = new javax.swing.JCheckBox();
        mPanelMigration = new javax.swing.JPanel();
        mRadioMigrationFitness = new javax.swing.JRadioButton();
        mRadioMigrationPopulation = new javax.swing.JRadioButton();
        mRadioMigrationScore = new javax.swing.JRadioButton();
        mRadioMigrationRandom = new javax.swing.JRadioButton();
        mRadioMigrationNone = new javax.swing.JRadioButton();
        mPanelEnvironment = new javax.swing.JPanel();
        mLabelTimeMaximum = new javax.swing.JLabel();
        SpinnerModel modelT = new SpinnerNumberModel(200000, 0, 1000000, 1);
        mSpinnerTimeMaximum = new JSpinner(modelT);
        SpinnerModel modelTempMin = new SpinnerNumberModel(-50, -200, 50, 1);
        mSpinnerTemperatureMinimum = new JSpinner(modelTempMin);
        mLabelTemperatureMinimum = new javax.swing.JLabel();
        mLabelTemperatureMaximum = new javax.swing.JLabel();
        SpinnerModel modelNabla = new SpinnerNumberModel(50, 0, 500, 1);
        mSpinnerTemperatureVariation = new JSpinner(modelNabla);
        SpinnerModel modelTempMax = new SpinnerNumberModel(150, 50, 250, 1);
        mSpinnerTemperatureMaximum = new JSpinner(modelTempMax);
        mLabelTemperatureVariation = new javax.swing.JLabel();
        mCheckFixedTemperature = new javax.swing.JCheckBox();
        mLabelForcingStrength = new javax.swing.JLabel();
        SpinnerModel modelBeta = new SpinnerNumberModel(25, 0, 500, 0.5);
        mSpinnerForcingStrength = new JSpinner(modelBeta);
        mPanelPopulation = new javax.swing.JPanel();
        mLabelCarryingCapacity = new javax.swing.JLabel();
        mLabelDiffusionParamater = new javax.swing.JLabel();
        mLabelNicheConstructionStength = new javax.swing.JLabel();
        mLabelMigrationProbability = new javax.swing.JLabel();
        mLabelSeedRate = new javax.swing.JLabel();
        SpinnerModel modelK = new SpinnerNumberModel(250, 0, 5000, 1);
        mSpinnerCarryingCapacity = new JSpinner(modelK);
        mCheckFixedPopulation = new javax.swing.JCheckBox();
        SpinnerModel modelL = new SpinnerNumberModel(0.99, 0, 1, 0.001);
        mSpinnerDiffusionParameter = new JSpinner(modelL);
        SpinnerModel modelAlpha = new SpinnerNumberModel(5, 0, 100, 0.5);
        mSpinnerNicheConstructionStrength = new JSpinner(modelAlpha);
        SpinnerModel modelMigProb = new SpinnerNumberModel(5, 0, 100, 0.5);
        mSpinnerMigrationProbability = new JSpinner(modelMigProb);
        SpinnerModel modelSeedRate = new SpinnerNumberModel(0.05, 0, 0.1, 0.01);
        mSpinnerSeedRate = new JSpinner(modelSeedRate);
        mPanelGenotype = new javax.swing.JPanel();
        SpinnerModel modelMuA = new SpinnerNumberModel(0.1, 0, 1, 0.01);
        mSpinnerAdaptionMutationRate = new JSpinner(modelMuA);
        mLabelAdaptionMutationRate = new javax.swing.JLabel();
        mLabelAdaptionMuStandDev = new javax.swing.JLabel();
        SpinnerModel modelMuAStandDev = new SpinnerNumberModel(0.05, 0, 0.5, 0.01);
        mSpinnerAdaptionMuStandDev = new JSpinner(modelMuAStandDev);
        mLabelEffectMutationRate = new javax.swing.JLabel();
        SpinnerModel modelMuE = new SpinnerNumberModel(0.1, 0, 1, 0.01);
        mSpinnerEffectMutationRate = new JSpinner(modelMuE);
        mLabelEffectMutationStandDev = new javax.swing.JLabel();
        SpinnerModel modelMuEStandDev = new SpinnerNumberModel(0.05, 0, 0.5, 0.01);
        mSpinnerEffectMutationStandDev = new JSpinner(modelMuEStandDev);
        mCheckFixedGenotype = new javax.swing.JCheckBox();

        mGroupMigration.add(mRadioMigrationFitness);
        mGroupMigration.add(mRadioMigrationPopulation);
        mGroupMigration.add(mRadioMigrationScore);
        mGroupMigration.add(mRadioMigrationRandom);
        mGroupMigration.add(mRadioMigrationNone);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Parameters");
        setResizable(false);

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mPanelOrganism.setBorder(javax.swing.BorderFactory.createTitledBorder("Organism"));

        mLabelDeathRate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelDeathRate.setText("Death Rate:");

        mLabelFitnessParabolaSpan.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelFitnessParabolaSpan.setText("Fitness Parabola Span:");

        mLabelAdaptionMaximum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelAdaptionMaximum.setText("Maximum Adaptation Level:");

        mLabelAdaptionMinimum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelAdaptionMinimum.setText("Minimum Adaptation Level:");

        mCheckFixedImpact.setSelected(true);
        mCheckFixedImpact.setText("Fixed Impact");
        mCheckFixedImpact.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelOrganismLayout = new javax.swing.GroupLayout(mPanelOrganism);
        mPanelOrganism.setLayout(mPanelOrganismLayout);
        mPanelOrganismLayout.setHorizontalGroup(
            mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelOrganismLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelOrganismLayout.createSequentialGroup()
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mLabelDeathRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelFitnessParabolaSpan, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelAdaptionMaximum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(mLabelAdaptionMinimum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mSpinnerAdaptionMinimum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                            .addComponent(mSpinnerAdaptionMaximum, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerFitnessParabolaSpan, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(mCheckFixedImpact, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mPanelOrganismLayout.setVerticalGroup(
            mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelOrganismLayout.createSequentialGroup()
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerAdaptionMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelAdaptionMinimum))
                .addGap(8, 8, 8)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerAdaptionMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelAdaptionMaximum))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerFitnessParabolaSpan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelFitnessParabolaSpan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelOrganismLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelDeathRate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mCheckFixedImpact))
        );

        mPanelMigration.setBorder(javax.swing.BorderFactory.createTitledBorder("Migration Scheme"));

        mRadioMigrationFitness.setText("Fitness    ");
        mRadioMigrationFitness.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioMigrationPopulation.setText("Population    ");
        mRadioMigrationPopulation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioMigrationScore.setText("Combined Score    ");
        mRadioMigrationScore.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mRadioMigrationRandom.setSelected(true);
        mRadioMigrationRandom.setText("Random    ");
        mRadioMigrationRandom.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mRadioMigrationRandom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mRadioMigrationRandomActionPerformed(evt);
            }
        });

        mRadioMigrationNone.setText("None");
        mRadioMigrationNone.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelMigrationLayout = new javax.swing.GroupLayout(mPanelMigration);
        mPanelMigration.setLayout(mPanelMigrationLayout);
        mPanelMigrationLayout.setHorizontalGroup(
            mPanelMigrationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelMigrationLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mRadioMigrationRandom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mRadioMigrationPopulation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mRadioMigrationFitness)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mRadioMigrationScore)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mRadioMigrationNone)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mPanelMigrationLayout.setVerticalGroup(
            mPanelMigrationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelMigrationLayout.createSequentialGroup()
                .addGroup(mPanelMigrationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mRadioMigrationFitness)
                    .addComponent(mRadioMigrationScore)
                    .addComponent(mRadioMigrationRandom)
                    .addComponent(mRadioMigrationNone)
                    .addComponent(mRadioMigrationPopulation))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelEnvironment.setBorder(javax.swing.BorderFactory.createTitledBorder("Environment"));

        mLabelTimeMaximum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelTimeMaximum.setText("Simulation Time:");

        mLabelTemperatureMinimum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelTemperatureMinimum.setText("Minimum Temperature:");

        mLabelTemperatureMaximum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelTemperatureMaximum.setText("Maximum Temperature:");

        mLabelTemperatureVariation.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelTemperatureVariation.setText("Temperature Variation:");

        mCheckFixedTemperature.setSelected(true);
        mCheckFixedTemperature.setText("Latitude Dependent Temperature");
        mCheckFixedTemperature.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        mLabelForcingStrength.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelForcingStrength.setText("Forcing Strength  (x 10E-5):");

        javax.swing.GroupLayout mPanelEnvironmentLayout = new javax.swing.GroupLayout(mPanelEnvironment);
        mPanelEnvironment.setLayout(mPanelEnvironmentLayout);
        mPanelEnvironmentLayout.setHorizontalGroup(
            mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelEnvironmentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mCheckFixedTemperature, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mPanelEnvironmentLayout.createSequentialGroup()
                        .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mLabelTimeMaximum)
                            .addComponent(mLabelTemperatureMaximum)
                            .addComponent(mLabelTemperatureMinimum)
                            .addComponent(mLabelTemperatureVariation)
                            .addComponent(mLabelForcingStrength, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mSpinnerForcingStrength, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerTemperatureVariation, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerTemperatureMaximum, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerTemperatureMinimum, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mSpinnerTimeMaximum, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))))
                .addContainerGap())
        );
        mPanelEnvironmentLayout.setVerticalGroup(
            mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelEnvironmentLayout.createSequentialGroup()
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelTimeMaximum)
                    .addComponent(mSpinnerTimeMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelTemperatureMinimum)
                    .addComponent(mSpinnerTemperatureMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelTemperatureMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerTemperatureMaximum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelTemperatureVariation, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mSpinnerTemperatureVariation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelEnvironmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelForcingStrength)
                    .addComponent(mSpinnerForcingStrength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mCheckFixedTemperature)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelPopulation.setBorder(javax.swing.BorderFactory.createTitledBorder("Population"));

        mLabelCarryingCapacity.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelCarryingCapacity.setText("Carrying Capacity:");

        mLabelDiffusionParamater.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelDiffusionParamater.setText("Diffusion Parameter:");

        mLabelNicheConstructionStength.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelNicheConstructionStength.setText("Niche Construction Strength  (x 10E-5):");

        mLabelMigrationProbability.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelMigrationProbability.setText("Migration Probability (x 10E-5):");

        mLabelSeedRate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelSeedRate.setText("Seed Rate:");

        mCheckFixedPopulation.setSelected(true);
        mCheckFixedPopulation.setText("Fixed Population Size");
        mCheckFixedPopulation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelPopulationLayout = new javax.swing.GroupLayout(mPanelPopulation);
        mPanelPopulation.setLayout(mPanelPopulationLayout);
        mPanelPopulationLayout.setHorizontalGroup(
            mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelPopulationLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mCheckFixedPopulation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mPanelPopulationLayout.createSequentialGroup()
                        .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mLabelCarryingCapacity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelDiffusionParamater, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelMigrationProbability, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelNicheConstructionStength, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelSeedRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mSpinnerMigrationProbability, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerNicheConstructionStrength, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerDiffusionParameter, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerCarryingCapacity, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerSeedRate, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        mPanelPopulationLayout.setVerticalGroup(
            mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelPopulationLayout.createSequentialGroup()
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelCarryingCapacity)
                    .addComponent(mSpinnerCarryingCapacity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelDiffusionParamater)
                    .addComponent(mSpinnerDiffusionParameter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelNicheConstructionStength)
                    .addComponent(mSpinnerNicheConstructionStrength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelMigrationProbability)
                    .addComponent(mSpinnerMigrationProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelPopulationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelSeedRate)
                    .addComponent(mSpinnerSeedRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(mCheckFixedPopulation)
                .addContainerGap())
        );

        mPanelGenotype.setBorder(javax.swing.BorderFactory.createTitledBorder("Genotype"));

        mLabelAdaptionMutationRate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelAdaptionMutationRate.setText("Adaptation Mutation Rate:");

        mLabelAdaptionMuStandDev.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelAdaptionMuStandDev.setText("A Distribution Stand. Dev:");

        mLabelEffectMutationRate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelEffectMutationRate.setText("Effect Mutation Rate:");

        mLabelEffectMutationStandDev.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelEffectMutationStandDev.setText("E Distribution Stand. Dev:");

        mCheckFixedGenotype.setText("Fixed Genotype");
        mCheckFixedGenotype.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mCheckFixedGenotype.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCheckFixedGenotypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mPanelGenotypeLayout = new javax.swing.GroupLayout(mPanelGenotype);
        mPanelGenotype.setLayout(mPanelGenotypeLayout);
        mPanelGenotypeLayout.setHorizontalGroup(
            mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGenotypeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelGenotypeLayout.createSequentialGroup()
                        .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mLabelEffectMutationRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelAdaptionMuStandDev, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelAdaptionMutationRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelEffectMutationStandDev, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mSpinnerEffectMutationRate, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mSpinnerAdaptionMuStandDev)
                            .addComponent(mSpinnerAdaptionMutationRate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                            .addComponent(mSpinnerEffectMutationStandDev))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(mCheckFixedGenotype, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mPanelGenotypeLayout.setVerticalGroup(
            mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGenotypeLayout.createSequentialGroup()
                .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerAdaptionMutationRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelAdaptionMutationRate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerAdaptionMuStandDev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelAdaptionMuStandDev))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerEffectMutationRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelEffectMutationRate))
                .addGap(7, 7, 7)
                .addGroup(mPanelGenotypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerEffectMutationStandDev, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelEffectMutationStandDev))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mCheckFixedGenotype)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mPanelMigration, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(mPanelEnvironment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mPanelOrganism, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(mPanelPopulation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mPanelGenotype, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mPanelPopulation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelEnvironment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelOrganism, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mPanelGenotype, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mPanelMigration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonOK)
                    .addComponent(mButtonApply)
                    .addComponent(mButtonCancel))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mRadioMigrationRandomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mRadioMigrationRandomActionPerformed
    }//GEN-LAST:event_mRadioMigrationRandomActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    private void mCheckFixedGenotypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCheckFixedGenotypeActionPerformed
        setGenotypeWidgets();
    }//GEN-LAST:event_mCheckFixedGenotypeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                new ParametersDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JCheckBox mCheckFixedGenotype;
    private javax.swing.JCheckBox mCheckFixedImpact;
    private javax.swing.JCheckBox mCheckFixedPopulation;
    private javax.swing.JCheckBox mCheckFixedTemperature;
    private javax.swing.ButtonGroup mGroupMigration;
    private javax.swing.JLabel mLabelAdaptionMaximum;
    private javax.swing.JLabel mLabelAdaptionMinimum;
    private javax.swing.JLabel mLabelAdaptionMuStandDev;
    private javax.swing.JLabel mLabelAdaptionMutationRate;
    private javax.swing.JLabel mLabelCarryingCapacity;
    private javax.swing.JLabel mLabelDeathRate;
    private javax.swing.JLabel mLabelDiffusionParamater;
    private javax.swing.JLabel mLabelEffectMutationRate;
    private javax.swing.JLabel mLabelEffectMutationStandDev;
    private javax.swing.JLabel mLabelFitnessParabolaSpan;
    private javax.swing.JLabel mLabelForcingStrength;
    private javax.swing.JLabel mLabelMigrationProbability;
    private javax.swing.JLabel mLabelNicheConstructionStength;
    private javax.swing.JLabel mLabelSeedRate;
    private javax.swing.JLabel mLabelTemperatureMaximum;
    private javax.swing.JLabel mLabelTemperatureMinimum;
    private javax.swing.JLabel mLabelTemperatureVariation;
    private javax.swing.JLabel mLabelTimeMaximum;
    private javax.swing.JPanel mPanelEnvironment;
    private javax.swing.JPanel mPanelGenotype;
    private javax.swing.JPanel mPanelMigration;
    private javax.swing.JPanel mPanelOrganism;
    private javax.swing.JPanel mPanelPopulation;
    private javax.swing.JRadioButton mRadioMigrationFitness;
    private javax.swing.JRadioButton mRadioMigrationNone;
    private javax.swing.JRadioButton mRadioMigrationPopulation;
    private javax.swing.JRadioButton mRadioMigrationRandom;
    private javax.swing.JRadioButton mRadioMigrationScore;
    private javax.swing.JSpinner mSpinnerAdaptionMaximum;
    private javax.swing.JSpinner mSpinnerAdaptionMinimum;
    private javax.swing.JSpinner mSpinnerAdaptionMuStandDev;
    private javax.swing.JSpinner mSpinnerAdaptionMutationRate;
    private javax.swing.JSpinner mSpinnerCarryingCapacity;
    private javax.swing.JSpinner mSpinnerDeathRate;
    private javax.swing.JSpinner mSpinnerDiffusionParameter;
    private javax.swing.JSpinner mSpinnerEffectMutationRate;
    private javax.swing.JSpinner mSpinnerEffectMutationStandDev;
    private javax.swing.JSpinner mSpinnerFitnessParabolaSpan;
    private javax.swing.JSpinner mSpinnerForcingStrength;
    private javax.swing.JSpinner mSpinnerMigrationProbability;
    private javax.swing.JSpinner mSpinnerNicheConstructionStrength;
    private javax.swing.JSpinner mSpinnerSeedRate;
    private javax.swing.JSpinner mSpinnerTemperatureMaximum;
    private javax.swing.JSpinner mSpinnerTemperatureMinimum;
    private javax.swing.JSpinner mSpinnerTemperatureVariation;
    private javax.swing.JSpinner mSpinnerTimeMaximum;
    // End of variables declaration//GEN-END:variables
}
