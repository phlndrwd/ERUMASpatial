/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * LegendPanel.java
 *
 * <p>This class is an extension of the standard JPanel class and represents the
 * legend for each CA grid. Has variables to control the colour and data
 * displayed as well as an overridden paint method to draw the legend.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui; // Part of the user interface package

import tools.ValueToColour; // Translates numerical values into colours
import java.awt.Color; // Uses colours
import java.awt.Graphics; // Used to handle graphics components
import javax.swing.JPanel; // The component this class extends

@SuppressWarnings( "serial" )
public class LegendPanel extends JPanel {

    private String mTitle; // The title of this legend
    private String mColour; // The colour of this legend
    private int mMinimumValue; // The minimum value of this legend
    private int mMaximumValue; // The maximum value of this legend

    @Override
    public void paint( Graphics g ) {

        super.paintComponent( g );
        Color c = null; // Colour object reference

        g.clearRect( 0, 0, 402, 50 ); // Clear old legend
        if( mTitle != null ) {
            if( mTitle.compareTo( "Nothing" ) != 0 ) {
                for( int i = 0; i < 400; i++ ) {
                    // Set chosen colour

                    if( mColour.compareTo( "jet" ) == 0 ) {
                        c = ValueToColour.toJet( i, 0, 400 );
                    } else if( mColour.compareTo( "plasma" ) == 0 ) {
                        c = ValueToColour.toPlasma( i, 0, 400 );
                    } else if( mColour.compareTo( "lava" ) == 0 ) {
                        c = ValueToColour.toLava( i, 0, 400 );
                    } else if( mColour.compareTo( "greenscale" ) == 0 ) {
                        c = ValueToColour.toGreenscale( i, 0, 400 );
                    } else if( mColour.compareTo( "greyscale" ) == 0 ) {
                        c = ValueToColour.toGreyscale( i, 0, 400 );
                    }
                    // Draw a colour representation of the point in the loop
                    g.setColor( c );
                    g.fillRect( i + 1, 1, i + 1, 25 );
                }
                // Draw a black border and the number zero at the left
                c = new Color( 0, 0, 0 );
                g.setColor( c );
                g.drawRect( 0, 0, 401, 25 );
                g.drawString( String.valueOf( mMinimumValue ), 0, 40 );
                // Ccorrect data type and positioning of max value                
                String maxString = String.valueOf( mMaximumValue );
                g.drawString( maxString, 400 - ( maxString.length() * 6 ), 40 );
                g.drawString( mTitle, 200 - ( ( mTitle.length() * 6 ) / 2 ), 40 );
            }
        }
    }

    /*
     * Setter for property title.
     * @param title New value of property title.
     */
    public void setTitle( String title ) {
        this.mTitle = title;
    }

    /*
     * Setter for property colour.
     * @param colour New value of property colour.
     */
    public void setColour( String colour ) {
        this.mColour = colour;
    }

    /*
     * Setter for property min.
     * @param minimumValue New value of property min.
     */
    public void setMinimumValue( int minimumValue ) {
        this.mMinimumValue = minimumValue;
    }

    /*
     * Setter for property max.
     * @param maximumValue New value of property max.
     */
    public void setMaximumValue( int maximumValue ) {
        this.mMaximumValue = maximumValue;
    }
}
