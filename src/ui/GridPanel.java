/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GridPanel.java
 *
 * <p>This class is an extension of the standard JPanel class and represents the
 * CA grid panel for real-time graphical output. It includes a buffered image 
 * which is passed as a parameter from the main interface so that changes to it
 * can be displayed on the panel.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui; // Part of the user interface package

import java.awt.Graphics; // Used to handle graphics components
import java.awt.image.BufferedImage; // To handle the passed image
import javax.swing.JPanel; // The component this class extends

@SuppressWarnings( "serial" )
public class GridPanel extends JPanel {

    private final BufferedImage mBufferedImage;

    /*
     * Constructs new panel with BufferedImage as a parameter.
     * @param bi The BufferedImage
     */
    public GridPanel( BufferedImage bufferedImage ) {
        this.mBufferedImage = bufferedImage;
    }

    /*
     * An override of the standard JPanel method, draws the image passed
     * as a parameter on the JPanel itself.
     */
    @Override
    protected void paintComponent( Graphics g ) {
        super.paintComponent( g );
        g.drawImage( mBufferedImage, 0, 0, null );
    }
}
