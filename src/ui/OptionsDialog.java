/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * OptionsDialog.java
 *
 * <p>This class is the interface from which all of the model variables and
 * settings can be configured by the user and saved to the global variables
 * object used by all the classes.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui; // Part of the user interface package

import java.awt.event.WindowAdapter; // Accesses window events
import java.awt.event.WindowEvent; // Handles window events
import java.awt.Frame; // Handles reference to the parent
import java.util.Random; // Creates a random number object
import javax.swing.JDialog; // Extends this class
import javax.swing.JSpinner; // Uses this component for numerical input
import javax.swing.SpinnerModel; // Holds spinner input model
import javax.swing.SpinnerNumberModel; // Type of spinner input model
import param.Options;

@SuppressWarnings( "serial" )
public class OptionsDialog extends JDialog {

    /*
     * Class constructor, centres on screen, creates and stores the variables
     * for the first time.
     */
    public OptionsDialog( Frame parent, boolean modal ) {

        super( parent, modal );
        initComponents();
        save(); // Store variables

        // Add a window listener so that the close button acts as cancelling
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );
    }

    /*
     * Resets the widgets back to their stored values, for interface cancel
     * events when saving altered values is not required.
     */
    private void reset() {
        int comboIndex = 0;

        for( int index = 0; index < mComboGridSize.getItemCount(); index++ ) {
            String selectedGridSizeOption = mComboGridSize.getItemAt( index );
            selectedGridSizeOption = selectedGridSizeOption.substring( 0, 2 ).replaceAll( "\\s", "" );

            if( Integer.parseInt( selectedGridSizeOption ) == Options.getGridSize() ) {
                comboIndex = index;
                break;
            }
        }
        mComboGridSize.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboLeftGridData.getItemCount(); index++ ) {
            String selectedLeftDataOption = mComboLeftGridData.getItemAt( index );

            if( selectedLeftDataOption.equalsIgnoreCase( Options.getLeftGridData() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboLeftGridData.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboLeftGridColour.getItemCount(); index++ ) {
            String selectedLeftColourOption = mComboLeftGridColour.getItemAt( index );

            if( selectedLeftColourOption.equalsIgnoreCase( Options.getLeftGridColour() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboLeftGridColour.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboRightGridData.getItemCount(); index++ ) {
            String selectedRightDataOption = mComboRightGridData.getItemAt( index );

            if( selectedRightDataOption.equalsIgnoreCase( Options.getRightGridData() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboRightGridData.setSelectedIndex( comboIndex );

        for( int index = 0; index < mComboRightGridColour.getItemCount(); index++ ) {
            String selectedRightColourOption = mComboRightGridColour.getItemAt( index );

            if( selectedRightColourOption.equalsIgnoreCase( Options.getRightGridColour() ) ) {
                comboIndex = index;
                break;
            }
        }
        mComboRightGridColour.setSelectedIndex( comboIndex );

        mSpinnerSamplingRate.setValue( Options.getSamplingRate() );
        mCheckCollectPopulationData.setSelected( Options.isCollectPopulationData() );
    }

    /*
     * Saves the values configured on the interface to the variables object
     */
    private void save() {
        // General model options
        Options.setSamplingRate( Integer.parseInt( mSpinnerSamplingRate.getValue().toString() ) );
        int newGridSize = Integer.parseInt( mComboGridSize.getSelectedItem().toString().substring( 0, 2 ).replaceAll( "\\s", "" ) );
        Options.setGridSize( newGridSize );
        Options.setLeftGridData( mComboLeftGridData.getSelectedItem().toString() );
        Options.setLeftGridColour( mComboLeftGridColour.getSelectedItem().toString().toLowerCase() );
        Options.setRightGridData( mComboRightGridData.getSelectedItem().toString() );
        Options.setRightGridColour( mComboRightGridColour.getSelectedItem().toString().toLowerCase() );
        Options.setRandom( new Random() ); // PJU FIX - Seed new random object
        Options.setCollectPopulationData( mCheckCollectPopulationData.isSelected() );
    }

    /*
     * Cancels alteration of variables and makes the dialog invisible
     */
    private void cancel() {
        setVisible( false );
        reset();
    }

    /*
     * Saves the variable configuration and makes the dialog invisible
     */
    private void ok() {
        setVisible( false );
        save();
    }

    /*
     * Saves the variable configuration
     */
    private void apply() {
        save();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mButtonOK = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonApply = new javax.swing.JButton();
        mPanelGeneral = new javax.swing.JPanel();
        mPanelLeftGrid = new javax.swing.JPanel();
        mLabelLeftDisplay = new javax.swing.JLabel();
        mComboLeftGridData = new javax.swing.JComboBox<>();
        mLabelLeftColour = new javax.swing.JLabel();
        mComboLeftGridColour = new javax.swing.JComboBox<>();
        mPanelRightGrid = new javax.swing.JPanel();
        mLabelRightDisplay = new javax.swing.JLabel();
        mLabelRightColour = new javax.swing.JLabel();
        mComboRightGridData = new javax.swing.JComboBox<>();
        mComboRightGridColour = new javax.swing.JComboBox<>();
        mComboGridSize = new javax.swing.JComboBox<>();
        mSpinnerGridSize = new javax.swing.JLabel();
        mLabelSamplingRate = new javax.swing.JLabel();
        SpinnerModel modelSamplingRate = new SpinnerNumberModel(100, 1, 10000, 1);
        mSpinnerSamplingRate = new JSpinner(modelSamplingRate);
        mCheckCollectPopulationData = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Options");
        setResizable(false);

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mPanelGeneral.setBorder(javax.swing.BorderFactory.createTitledBorder("General"));

        mPanelLeftGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Left Grid"));

        mLabelLeftDisplay.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelLeftDisplay.setText("Display:");

        mComboLeftGridData.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Temperature", "Fitness", "Effect", "Adaptation", "Population Size", "Nothing" }));

        mLabelLeftColour.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelLeftColour.setText("Colour:");

        mComboLeftGridColour.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Jet", "Plasma", "Lava", "Greenscale", "Greyscale" }));

        javax.swing.GroupLayout mPanelLeftGridLayout = new javax.swing.GroupLayout(mPanelLeftGrid);
        mPanelLeftGrid.setLayout(mPanelLeftGridLayout);
        mPanelLeftGridLayout.setHorizontalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelLeftGridLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mLabelLeftColour)
                    .addComponent(mLabelLeftDisplay))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mComboLeftGridData, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mComboLeftGridColour, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        mPanelLeftGridLayout.setVerticalGroup(
            mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelLeftGridLayout.createSequentialGroup()
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mComboLeftGridData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelLeftDisplay))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelLeftGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mComboLeftGridColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelLeftColour))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mPanelRightGrid.setBorder(javax.swing.BorderFactory.createTitledBorder("Right Grid"));

        mLabelRightDisplay.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelRightDisplay.setText("Display:");

        mLabelRightColour.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelRightColour.setText("Colour:");

        mComboRightGridData.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Temperature", "Fitness", "Effect", "Adaptation", "Population Size", "Nothing" }));
        mComboRightGridData.setSelectedIndex( 1 );

        mComboRightGridColour.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Jet", "Plasma", "Lava", "Greenscale", "Greyscale" }));
        mComboRightGridColour.setSelectedIndex( 4 );

        javax.swing.GroupLayout mPanelRightGridLayout = new javax.swing.GroupLayout(mPanelRightGrid);
        mPanelRightGrid.setLayout(mPanelRightGridLayout);
        mPanelRightGridLayout.setHorizontalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelRightGridLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mLabelRightDisplay)
                    .addComponent(mLabelRightColour))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mComboRightGridColour, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mComboRightGridData, 0, 186, Short.MAX_VALUE))
                .addContainerGap())
        );
        mPanelRightGridLayout.setVerticalGroup(
            mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelRightGridLayout.createSequentialGroup()
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mComboRightGridData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelRightDisplay))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelRightGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mComboRightGridColour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelRightColour))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mComboGridSize.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "1 x 1", "2 x 2", "4 x 4", "5 x 5", "8 x 8", "10 x 10", "20 x 20" }));
        mComboGridSize.setSelectedIndex( 5 );

        mSpinnerGridSize.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mSpinnerGridSize.setText("Grid Size:");

        mLabelSamplingRate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelSamplingRate.setText("Sampling Rate:");

        mCheckCollectPopulationData.setText("Collect Individual Population Data");

        javax.swing.GroupLayout mPanelGeneralLayout = new javax.swing.GroupLayout(mPanelGeneral);
        mPanelGeneral.setLayout(mPanelGeneralLayout);
        mPanelGeneralLayout.setHorizontalGroup(
            mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelGeneralLayout.createSequentialGroup()
                        .addComponent(mSpinnerGridSize)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mComboGridSize, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mPanelGeneralLayout.createSequentialGroup()
                        .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mPanelGeneralLayout.createSequentialGroup()
                                .addComponent(mLabelSamplingRate)
                                .addGap(18, 18, 18)
                                .addComponent(mSpinnerSamplingRate, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mCheckCollectPopulationData, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mPanelRightGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        mPanelGeneralLayout.setVerticalGroup(
            mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelGeneralLayout.createSequentialGroup()
                .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerGridSize)
                    .addComponent(mComboGridSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mPanelLeftGrid, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mPanelRightGrid, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerSamplingRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelSamplingRate)
                    .addComponent(mCheckCollectPopulationData))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mPanelGeneral, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 363, Short.MAX_VALUE)
                        .addComponent(mButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mPanelGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonOK)
                    .addComponent(mButtonApply)
                    .addComponent(mButtonCancel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                new OptionsDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JCheckBox mCheckCollectPopulationData;
    private javax.swing.JComboBox<String> mComboGridSize;
    private javax.swing.JComboBox<String> mComboLeftGridColour;
    private javax.swing.JComboBox<String> mComboLeftGridData;
    private javax.swing.JComboBox<String> mComboRightGridColour;
    private javax.swing.JComboBox<String> mComboRightGridData;
    private javax.swing.JLabel mLabelLeftColour;
    private javax.swing.JLabel mLabelLeftDisplay;
    private javax.swing.JLabel mLabelRightColour;
    private javax.swing.JLabel mLabelRightDisplay;
    private javax.swing.JLabel mLabelSamplingRate;
    private javax.swing.JPanel mPanelGeneral;
    private javax.swing.JPanel mPanelLeftGrid;
    private javax.swing.JPanel mPanelRightGrid;
    private javax.swing.JLabel mSpinnerGridSize;
    private javax.swing.JSpinner mSpinnerSamplingRate;
    // End of variables declaration//GEN-END:variables
}
