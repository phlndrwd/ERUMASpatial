/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * DoubleDatum.java
 *
 * <p>This class is used to collect the data from a single variable of the 
 * system.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package data; // Part of the data package

public class DoubleDatum {

    private String mName; // The name of the variable
    private final double[][] mData; // The two dimensional array that will hold the data.

    /*
     * Class constructor
     * @param name The name of the variable
     * @param rows The number of rows
     * @param columns The number of columns
     */
    public DoubleDatum( String name, int rows, int columns ) {

        mName = name;
        mData = new double[ rows ][ columns ];
    }

    /*
     * Add data to the array.
     * @param row The row index
     * @param column The column index
     * @param value The value to write.
     */
    public void addData( int row, int column, double value ) {
        mData[ row ][ column ] = value;
    }

    /*
     * Getter for property name.
     * @return Value of property mName.
     */
    public String getName() {
        return mName;
    }

    /*
     * Setter for property name.
     * @param name New value of property mName.
     */
    public void setName( String name ) {
        mName = name;
    }

    /*
     * Indexed getter for property data.
     * @param row Index of the property.
     * @param column Index of the property.
     * @return Value of the property at the row and column in the data matrix.
     */
    public double getData( int row, int column ) {
        return mData[ row ][ column ];
    }
}
