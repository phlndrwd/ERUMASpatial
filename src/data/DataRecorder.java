/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * DataRecorder.java
 *
 * <p>This class is held by the Environment class and acts as a single container
 * for the drawing and logging objects.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package data; // Part of the data package

import tools.GridDraw;
import model.Population;
import ui.MainInterface;
import param.Parameters;
import param.Constants;
import param.Options;

public class DataRecorder {

    private final GridDraw mDraw; // The drawing object
    private final DataWriter mDataWriter; // The data logging object
    
    private double mPopulationSize; // Temporary storage of the population size
    private double mTempLocal; // Temporary storage of the population temperature
    private double mMeanAdaptation; // Mean adaptation of each opulation
    private double mMeanEffect; // Mean effect of each opulation
    private double mMeanFitness; // Mean fitness of each opulation
    
    private int mSampleIndex; // Matrix column counter for collecting data
    private int mPopulationIndex; // Matrix row counter for collecting data

    /*
     * Object constructor
     * @param parent Allows access to public properties of parent object.
     */
    public DataRecorder( MainInterface parent ) {

        mDraw = new GridDraw( parent.mLeftImageGraphics, parent.mRightImageGraphics, parent.mPanelLeftGrid, parent.mPanelRightGrid );
        mDataWriter = new DataWriter();
        
        mSampleIndex = 0;
        mPopulationIndex = 0;
    }

    /*
     * This method is called from the Environment object everytime data needs to
     * be collected and the display updated.
     */
    public void updateOutput( Population[][] populations, int t ) {

        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {
                mPopulationIndex = ( i * Options.getGridSize() ) + j;

                // Update the population statistics
                populations[i][j].updateFitnesses();
                populations[i][j].calcMeanData( mDataWriter, mPopulationIndex, mSampleIndex );

                mTempLocal = populations[i][j].getTempLocal();
                mMeanFitness = populations[i][j].getMeanFitness();
                mMeanEffect = populations[i][j].getMeanEffect();
                mMeanAdaptation = populations[i][j].getMeanAdaptation();
                mPopulationSize = populations[i][j].getPopulationSize();

                // Add the data to the logging objects
                mDataWriter.addTimeData( mSampleIndex, t );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.POPULATIONSIZE.ordinal(), mPopulationIndex, mSampleIndex, mPopulationSize );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.TEMPERATUREFORCING.ordinal(), mPopulationIndex, mSampleIndex, populations[i][j].getTempForcing() );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.TEMPERATURELOCAL.ordinal(), mPopulationIndex, mSampleIndex, mTempLocal );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.MEANADAPTATION.ordinal(), mPopulationIndex, mSampleIndex, mMeanAdaptation );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.MEANEFFECT.ordinal(), mPopulationIndex, mSampleIndex, mMeanEffect );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.MEANFITNESS.ordinal(), mPopulationIndex, mSampleIndex, mMeanFitness );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.EINCREASINGCOUNT.ordinal(), mPopulationIndex, mSampleIndex, populations[i][j].getEIncreasingCount() );
                mDataWriter.addModelData( Constants.eVariableFileNamesEnum.EDECREASINGCOUNT.ordinal(), mPopulationIndex, mSampleIndex, populations[i][j].getEDecreasingCount() );

                // Draw the CA grids
                drawGrid( "left", Options.getLeftGridColour(), Options.getLeftGridData(), i, j );
                drawGrid( "right", Options.getRightGridColour(), Options.getRightGridData(), i, j );

            } // End j
        } // End i
        mDraw.drawImages();
        mDraw.repaintGridPanels();
        mSampleIndex++;
    }


    /*
     * Universal method for drawing the left and right-hand CA grid images
     */
    private void drawGrid( String side, String colour, String data, int i, int j ) {

        if( data.compareTo( "Nothing" ) != 0 ) {
            if( data.compareTo( "Temperature" ) == 0 ) {
                if( Parameters.isFixedTemperature() ) {
                    mDraw.drawGrid( side, mTempLocal, Parameters.getTemperatureMinimum(), Parameters.getTemperatureMaximum(), colour, i, j );
                } else {
                    mDraw.drawGrid( side, mTempLocal, Parameters.getTemperatureMinimum(), Parameters.getTemperatureMaximum() + Parameters.getTemperatureVariation(), colour, i, j );
                }
            } else if( data.compareTo( "Fitness" ) == 0 ) {
                mDraw.drawGrid( side, mMeanFitness, 0, 1, colour, i, j );
            } else if( data.compareTo( "Effect" ) == 0 ) {
                mDraw.drawGrid( side, mMeanEffect, -1, 1, colour, i, j );
            } else if( data.compareTo( "Adaptation" ) == 0 ) {
                if( Parameters.isFixedTemperature() ) {
                    mDraw.drawGrid( side, mMeanAdaptation, Parameters.getTemperatureMinimum(), Parameters.getTemperatureMaximum(), colour, i, j );
                } else {
                    mDraw.drawGrid( side, mMeanAdaptation, Parameters.getTemperatureMinimum(), Parameters.getTemperatureMaximum() + Parameters.getTemperatureVariation(), colour, i, j );
                }
            } else if( data.compareTo( "Population Size" ) == 0 ) {
                mDraw.drawGrid( side, mPopulationSize, 0, Parameters.getCarryingCapacity(), colour, i, j );
            }
        }
    }

    /*
     * Called at the end of the data collection
     */
    public void endOutput() {
        mDataWriter.toFile();
    }
}
