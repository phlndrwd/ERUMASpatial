/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * DataWriter.java
 *
 * <p>This class builds an array of datums object to handle each of the
 * collected variables. It also takes care of writing these to files aswell as
 * the model variables and settings.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package data; // Part of the data package

import java.io.BufferedWriter; // For out-file streams
import param.Constants;
import java.io.File; // For file creation
import java.io.FileWriter;
import java.io.IOException; // For handling exceptions
import java.text.DateFormat; // For producing a formatted date
import java.text.SimpleDateFormat; // For creating the date format
import java.util.ArrayList; // For handling the array of variable names
import java.util.Arrays;
import java.util.Date; // For creating a date object
import param.Options;
import param.Parameters; // Used the model variables

public class DataWriter {

    private final DoubleDatum[] mModelDatums; // The array of objects to handle each variable
    private final DoubleDatum[] mPopulationDatums;
    private final IntDatum mTimeDatum;

    private final ArrayList<String> mParameterNames;
    private final ArrayList<String> mParameterValues;

    private final int mTotalPopulations; // The m dimension of the data matrices
    private final int mTotalSamples; // The n dimension of the data matrices

    private int mWidthOfWidestParameterSet;

    /*
     * Class constructor
     */
    public DataWriter() {

        mTotalSamples = Math.round( Parameters.getTimeMaximum() / Options.getSamplingRate() ) + 1;
        mTotalPopulations = Math.round( ( float )Math.pow( Options.getGridSize(), 2 ) );

        mModelDatums = new DoubleDatum[ Constants.cVariableFileNames.length ];
        for( int i = 0; i < Constants.cVariableFileNames.length; i++ ) {
            mModelDatums[ i ] = new DoubleDatum( Constants.cVariableFileNames[ i ], mTotalPopulations, mTotalSamples );
        }

        mPopulationDatums = new DoubleDatum[ mTotalPopulations ];
        for( int i = 0; i < mTotalPopulations; i++ ) {
            // Initialised to 1 above carrying capacity to account for ArrayOutOfBounds exception when running with variable population.
            mPopulationDatums[ i ] = new DoubleDatum( "Population_" + i, Parameters.getCarryingCapacity() + 1, mTotalSamples );
        }

        mTimeDatum = new IntDatum( "Time", mTotalSamples );

        mParameterNames = new ArrayList<>();
        mParameterValues = new ArrayList<>();

        mWidthOfWidestParameterSet = 0;
    }

    /*
     * The method used to add data to the matrices
     * @param datum Integer representation of the variable this data represents
     * @param populationIndex The vertical position of the matrix
     * @param sampleIndex The horizontal position of the matrix
     * @param value The value to write at the specified position
     */
    public void addModelData( int datum, int populationIndex, int sampleIndex, double value ) {

        mModelDatums[ datum ].addData( populationIndex, sampleIndex, value );
    }

    public void addPopulationData( int populationIndex, int organismIndex, int sampleIndex, double value ) {

        mPopulationDatums[ populationIndex ].addData( organismIndex, sampleIndex, value );
    }

    public void addTimeData( int index, int value ) {
        mTimeDatum.addData( index, value );
    }

    /*
     * Given a specified data format, return the data in that format
     * @param format The string representation of the required data format
     * @return The formatted date string
     */
    private String getDateTime( String format ) {
        DateFormat dateFormat = new SimpleDateFormat( format );
        Date date = new Date();
        return dateFormat.format( date );
    }

    /*
     * The method called once all the data collection has been performed in order
     * to write everything to files
     */
    public void toFile() {

        String path = Constants.cOutputDir + getDateTime( "yyyy-MM-dd_HH-mm" ) + "/";
        String fileName;
        BufferedWriter out;

        new File( Constants.cOutputDir ).mkdir();
        new File( path ).mkdir();

        for( DoubleDatum mModelDatum: mModelDatums ) {
            try {
                // PJU FIX - Optional file extension
                fileName = mModelDatum.getName() + ".csv";
                out = new BufferedWriter( new FileWriter( path + fileName ) );
                Double dataItem;
                for( int row = 0; row < mTotalPopulations; row++ ) {
                    for( int column = 0; column < mTotalSamples - 1; column++ ) {
                        dataItem = mModelDatum.getData( row, column );
                        out.write( dataItem.toString() + ", " );
                    }
                    dataItem = mModelDatum.getData( row, mTotalSamples - 1 );
                    out.write( dataItem.toString() );
                    out.newLine();
                }
                out.close();
            } catch( IOException e ) {
                System.out.println( Arrays.toString( e.getStackTrace() ) );
            }
        }

        try {
            // PJU FIX - Optional file extension
            fileName = "Time.csv";

            out = new BufferedWriter( new FileWriter( path + fileName ) );
            Integer dataItem;

            for( int index = 0; index < mTotalSamples - 1; index++ ) {
                dataItem = mTimeDatum.getData( index );
                out.write( dataItem.toString() + ", " );
            }

            dataItem = mTimeDatum.getData( mTotalSamples - 1 );
            out.write( dataItem.toString() );
            out.newLine();

            out.close();

        } catch( IOException e ) {
            System.out.println( Arrays.toString( e.getStackTrace() ) );
        }

        try {
            // Write parameter configuration to file
            createParameterStringVectors();

            fileName = "Parameters.txt";
            out = new BufferedWriter( new FileWriter( path + fileName ) );
            out.write( "ERUMASpatial " + Constants.cVersionNumber );
            out.newLine();
            out.write( getDateTime( "EEE MMM dd HH:mm:ss zzz yyyy" ) );
            out.newLine();
            for( int lineNumber = 0; lineNumber < mParameterNames.size(); lineNumber++ ) {
                out.write( getParameterLine( lineNumber ) );
                out.newLine();
            }

            out.close();
        } catch( IOException e ) {
            System.out.println( Arrays.toString( e.getStackTrace() ) );
        }

        if( Options.isCollectPopulationData() == true ) {
            for( int i = 0; i < mPopulationDatums.length; i++ ) {

                String populationPath = path + "Population_" + ( i + 1 ) + "/";
                new File( populationPath ).mkdir();

                try {
                    fileName = "Adaption.csv";
                    out = new BufferedWriter( new FileWriter( populationPath + fileName ) );
                    Double dataItem;
                    for( int row = 0; row < Parameters.getCarryingCapacity(); row++ ) {

                        for( int column = 0; column < mTotalSamples - 1; column++ ) {
                            dataItem = mPopulationDatums[ i ].getData( row, column );
                            out.write( dataItem.toString() + ", " );
                        }
                        dataItem = mPopulationDatums[ i ].getData( row, mTotalSamples - 1 );
                        out.write( dataItem.toString() );
                        out.newLine();
                    }
                    out.close();

                } catch( IOException e ) {
                    System.out.println( Arrays.toString( e.getStackTrace() ) );
                }
            }
        }
    }

    private void createParameterStringVectors() {
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "Simulation Time" );
        mParameterValues.add( Integer.toString( Parameters.getTimeMaximum() ) );
        mParameterNames.add( "Minimum Temperature" );
        mParameterValues.add( Integer.toString( Parameters.getTemperatureMinimum() ) );
        mParameterNames.add( "Maximum Temperature" );
        mParameterValues.add( Integer.toString( Parameters.getTemperatureMaximum() ) );
        mParameterNames.add( "Temperature Varation (Nabla)" );
        mParameterValues.add( Integer.toString( Parameters.getTemperatureVariation() ) );
        mParameterNames.add( "Forcing Strength (Beta)" );
        mParameterValues.add( Double.toString( Parameters.getForcingStrength() ) );
        mParameterNames.add( "Is Fixed Temperature" );
        mParameterValues.add( Boolean.toString( Parameters.isFixedTemperature() ) );
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "Carrying Capacity (K)" );
        mParameterValues.add( Integer.toString( Parameters.getCarryingCapacity() ) );
        mParameterNames.add( "Diffusion Parameter" );
        mParameterValues.add( Double.toString( Parameters.getDiffusionParameter() ) );
        mParameterNames.add( "Niche Construction Strength (Alpha)" );
        mParameterValues.add( Double.toString( Parameters.getNicheConstructionStrength() ) );
        mParameterNames.add( "Migration Probability (Rho)" );
        mParameterValues.add( Double.toString( Parameters.getMigrationProbability() ) );
        mParameterNames.add( "Seed Rate (Delta)" );
        mParameterValues.add( Double.toString( Parameters.getSeedRate() ) );
        mParameterNames.add( "Is Fixed Population" );
        mParameterValues.add( Boolean.toString( Parameters.isFixedPopulation() ) );
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "Adaption Minimum" );
        mParameterValues.add( Integer.toString( Parameters.getAdaptionMinimum() ) );
        mParameterNames.add( "Adaption Maximum" );
        mParameterValues.add( Integer.toString( Parameters.getAdaptionMaximum() ) );
        mParameterNames.add( "Fitness Parabola Span (Lambda)" );
        mParameterValues.add( Double.toString( Parameters.getFitnessParabolaSpan() ) );
        mParameterNames.add( "Death Rate (Gamma)" );
        mParameterValues.add( Double.toString( Parameters.getDeathRate() ) );
        mParameterNames.add( "Is Fixed Impact" );
        mParameterValues.add( Boolean.toString( Parameters.isFixedImpact() ) );
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "Adaption Mutation Rate" );
        mParameterValues.add( Double.toString( Parameters.getMutationRate( 0 ) ) );
        mParameterNames.add( "Effect Mutation Rate" );
        mParameterValues.add( Double.toString( Parameters.getMutationRate( 1 ) ) );
        mParameterNames.add( "Adaption Mutation Standard Deviation" );
        mParameterValues.add( Double.toString( Parameters.getMutationStandDev( 0 ) ) );
        mParameterNames.add( "Effect Mutation Standard Deviation" );
        mParameterValues.add( Double.toString( Parameters.getMutationStandDev( 1 ) ) );
        mParameterNames.add( "Is Fixed Genotype" );
        mParameterValues.add( Boolean.toString( Parameters.isFixedGenotype() ) );
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "Migration Scheme" );
        mParameterValues.add( Integer.toString( Parameters.getMigrationScheme() ) );
        mParameterNames.add( "-" );
        mParameterValues.add( "-" );
        mParameterNames.add( "SamplingRate" );
        mParameterValues.add( Integer.toString( Options.getSamplingRate() ) );
        mParameterNames.add( "GridSize" );
        mParameterValues.add( Integer.toString( Options.getGridSize() ) );

        for( int parameterSetIndex = 0; parameterSetIndex < mParameterNames.size(); ++parameterSetIndex ) {

            int widthOfParameterSet = mParameterNames.get( parameterSetIndex ).length() + mParameterValues.get( parameterSetIndex ).length();

            if( widthOfParameterSet > mWidthOfWidestParameterSet ) {

                mWidthOfWidestParameterSet = widthOfParameterSet;
            }
        }
        mWidthOfWidestParameterSet = mWidthOfWidestParameterSet + Constants.cMinimumParameterNamesSpacing + Constants.cParameterNamesValueDelimiter.length();
    }

    private String getParameterLine( int lineNumber ) {

        String parameterLine;

        if( mParameterNames.get( lineNumber ).equals( "-" ) == true ) {
            parameterLine = "";
            for( int i = 0; i < mWidthOfWidestParameterSet; i++ ) {
                parameterLine = parameterLine.concat( "-" );
            }
        } else {
            int numberOfSpaces = mWidthOfWidestParameterSet - ( mParameterNames.get( lineNumber ).length() + Constants.cParameterNamesValueDelimiter.length() + mParameterValues.get( lineNumber ).length() );
            parameterLine = mParameterNames.get( lineNumber );

            for( int i = 0; i < numberOfSpaces; i++ ) {
                parameterLine = parameterLine.concat( Constants.cParameterNameValueSpacer );
            }
            parameterLine = parameterLine.concat( Constants.cParameterNamesValueDelimiter );
            parameterLine = parameterLine.concat( mParameterValues.get( lineNumber ) );
        }
        return parameterLine;
    }
}
