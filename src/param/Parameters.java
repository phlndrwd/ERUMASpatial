/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parameters.java
 *
 * <p>This class holds all of the model parameters configured from the 
 * parameters dialog interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package param;

public class Parameters {

    /*
     * Environmental variables
     */
    private static int mTimeMaximum; // Maximum simulation time
    private static int mTemperatureMinimum; // Minimum temperature at the 'poles'
    private static int mTemperatureMaximum; // Maximum temperature at the 'poles'
    private static double mTemperatureIncrement;  // Temperature increment at each timestep
    private static boolean mIsFixedTemperature; // Controls use of latitude dependent temperatures
    private static int mTemperatureVariation; // The range of temperature forcing between equator and poles
    private static double mForcingStrength; // External forcing strength
    /*
     * Population variables
     */
    private static int mCarryingCapacity; // Maximum population carrying capacity
    private static boolean mIsFixedPopulation; // Controls variable or fixed population
    private static double mDiffusionParameter; // Multiple population leakage/diffusion parameter
    private static double mNicheConstructionStrength; // Niche construction strength
    private static double mMigrationProbability; // Probability of migration
    private static double mSeedRate; // Controls rate of population seeding
    /*
     * Organismic variables
     */
    private static int mAdaptionMinimum; // Lower bound of habitability
    private static int mAdaptionMaximum; // Upper bound of habitability
    private static boolean mIsFixedImpact; // Variable or fixed impact on temperature
    private static double mFitnessParabolaSpan; // Width of fitness parabola
    private static double mDeathRate; // Rate of death
    private static boolean mIsFixedGenotype; // Controls application of mutation
    private static double mMutationRate[] = new double[ 2 ]; // Mutation rate
    private static double mMutationStandDev[] = new double[ 2 ]; // Distribution stand. dev.
    private static int mMigrationScheme; // The method of migration used

    /*
     * Getter for property mTimeMaximum.
     * @return Value of property mTimeMaximum.
     */
    public static int getTimeMaximum() {
        return mTimeMaximum;
    }

    /*
     * Setter for property mTimeMaximum.
     * @param timeMaximum New value of property mTimeMaximum.
     */
    public static void setTimeMaximum( int timeMaximum ) {
        mTimeMaximum = timeMaximum;
    }

    /*
     * Getter for property mTemperatureMinimum.
     * @return Value of property mTemperatureMinimum.
     */
    public static int getTemperatureMinimum() {
        return mTemperatureMinimum;
    }

    /*
     * Setter for property mTemperatureMinimum.
     * @param temperatureMinimum New value of property mTemperatureMinimum.
     */
    public static void setTemperatureMinimum( int temperatureMinimum ) {
        mTemperatureMinimum = temperatureMinimum;
    }

    /*
     * Getter for property mTemperatureMaximum.
     * @return Value of property mTemperatureMaximum.
     */
    public static int getTemperatureMaximum() {
        return mTemperatureMaximum;
    }

    /*
     * Setter for property mTemperatureMaximum.
     * @param temperatureMaximum New value of property mTemperatureMaximum.
     */
    public static void setTemperatureMaximum( int temperatureMaximum ) {
        mTemperatureMaximum = temperatureMaximum;
    }

    /*
     * Getter for property mTemperatureIncrement.
     * @return Value of property mTemperatureIncrement.
     */
    public static double getTemperatureIncrement() {
        return mTemperatureIncrement;
    }

    /*
     * Setter for property mTemperatureIncrement.
     * @param temperatureIncrement New value of property mTemperatureIncrement.
     */
    public static void setTemperatureIncrement( double temperatureIncrement ) {
        mTemperatureIncrement = temperatureIncrement;
    }

    /*
     * Getter for property mIsFixedTemperature.
     * @return Value of property mIsFixedTemperature.
     */
    public static boolean isFixedTemperature() {
        return mIsFixedTemperature;
    }

    /*
     * Setter for property mIsFixedTemperature.
     * @param isFixedTemperature New value of property mIsFixedTemperature.
     */
    public static void setFixedTemperature( boolean isFixedTemperature ) {
        mIsFixedTemperature = isFixedTemperature;
    }

    /*
     * Getter for property mTemperatureVariation.
     * @return Value of property mTemperatureVariation.
     */
    public static int getTemperatureVariation() {
        return mTemperatureVariation;
    }

    /*
     * Setter for property mTemperatureVariation.
     * @param temperatureVariation New value of property mTemperatureVariation.
     */
    public static void setTemperatureVariation( int temperatureVariation ) {
        mTemperatureVariation = temperatureVariation;
    }

    /*
     * Getter for property mCarryingCapacity.
     * @return Value of property mCarryingCapacity.
     */
    public static int getCarryingCapacity() {
        return mCarryingCapacity;
    }

    /*
     * Setter for property mCarryingCapacity.
     * @param carryingCapacity New value of property mCarryingCapacity.
     */
    public static void setCarryingCapacity( int carryingCapacity ) {
        mCarryingCapacity = carryingCapacity;
    }

    /*
     * Getter for property mIsFixedPopulation.
     * @return Value of property mIsFixedPopulation.
     */
    public static boolean isFixedPopulation() {
        return mIsFixedPopulation;
    }

    /*
     * Setter for property mIsFixedPopulation.
     * @param isFixedPopulation New value of property mIsFixedPopulation.
     */
    public static void setFixedPopulation( boolean isFixedPopulation ) {
        mIsFixedPopulation = isFixedPopulation;
    }

    /*
     * Getter for property mDiffusionParameter.
     * @return Value of property mDiffusionParameter.
     */
    public static double getDiffusionParameter() {
        return mDiffusionParameter;
    }

    /*
     * Setter for property mDiffusionParameter.
     * @param diffusionParameter New value of property mDiffusionParameter.
     */
    public static void setDiffusionParameter( double diffusionParameter ) {
        mDiffusionParameter = diffusionParameter;
    }

    /*
     * Getter for property mNicheConstructionStrength.
     * @return Value of property mNicheConstructionStrength.
     */
    public static double getNicheConstructionStrength() {
        return mNicheConstructionStrength;
    }

    /*
     * Setter for property mNicheConstructionStrength.
     * @param nicheConstructionStrength New value of property mNicheConstructionStrength.
     */
    public static void setNicheConstructionStrength( double nicheConstructionStrength ) {
        mNicheConstructionStrength = nicheConstructionStrength;
    }

    /*
     * Getter for property mForcingStrength.
     * @return Value of property mForcingStrength.
     */
    public static double getForcingStrength() {
        return mForcingStrength;
    }

    /*
     * Setter for property mForcingStrength.
     * @param forcingStrength New value of property mForcingStrength.
     */
    public static void setForcingStrength( double forcingStrength ) {
        mForcingStrength = forcingStrength;
    }

    /*
     * Getter for property mMigrationProbability.
     * @return Value of property mMigrationProbability.
     */
    public static double getMigrationProbability() {
        return mMigrationProbability;
    }

    /*
     * Setter for property mMigrationProbability.
     * @param migrationProbability New value of property mMigrationProbability.
     */
    public static void setMigrationProbability( double migrationProbability ) {
        mMigrationProbability = migrationProbability;
    }

    /*
     * Getter for property mMigrationScheme.
     * @return Value of property mMigrationScheme.
     */
    public static int getMigrationScheme() {
        return mMigrationScheme;
    }

    /*
     * Setter for property mMigrationScheme.
     * @param migrationScheme New value of property mMigrationScheme.
     */
    public static void setMigrationScheme( int migrationScheme ) {
        mMigrationScheme = migrationScheme;
    }

    /*
     * Getter for property mSeedRate.
     * @return Value of property mSeedRate.
     */

    public static double getSeedRate() {
        return mSeedRate;
    }

    /*
     * Setter for property mSeedRate.
     * @param seedRate New value of property mSeedRate.
     */
    public static void setSeedRate( double seedRate ) {
        mSeedRate = seedRate;
    }

    /*
     * Getter for property mAdaptionMinimum.
     * @return Value of property mAdaptionMinimum.
     */
    public static int getAdaptionMinimum() {
        return mAdaptionMinimum;
    }

    /*
     * Setter for property mAdaptionMinimum.
     * @param adaptionMinimum New value of property mAdaptionMinimum.
     */
    public static void setAdaptionMinimum( int adaptionMinimum ) {
        mAdaptionMinimum = adaptionMinimum;
    }

    /*
     * Getter for property mAdaptionMaximum.
     * @return Value of property mAdaptionMaximum.
     */
    public static int getAdaptionMaximum() {
        return mAdaptionMaximum;
    }

    /*
     * Setter for property mAdaptionMaximum.
     * @param adaptionMaximum New value of property mAdaptionMaximum.
     */
    public static void setAdaptionMaximum( int adaptionMaximum ) {
        mAdaptionMaximum = adaptionMaximum;
    }

    /*
     * Indexed getter for property mMutationRate.
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static double getMutationRate( int index ) {
        return mMutationRate[ index ];
    }

    /*
     * Indexed setter for property mMutationRate.
     * @param index Index of the property.
     * @param mutationRate New value of the property at <CODE>index</CODE>.
     */
    public static void setMutationRate( int index, double mutationRate ) {
        mMutationRate[ index ] = mutationRate;
    }

    /*
     * Indexed getter for property muStandDev.
     * @param index Index of the property.
     * @return Value of the property at <CODE>index</CODE>.
     */
    public static double getMutationStandDev( int index ) {
        return mMutationStandDev[ index ];
    }

    /*
     * Indexed setter for property muStandDev.
     * @param index Index of the property.
     * @param muStandDev New value of the property at <CODE>index</CODE>.
     */
    public static void setMutationStandDev( int index, double muStandDev ) {
        mMutationStandDev[ index ] = muStandDev;
    }

    /*
     * Getter for property mFitnessParabolaSpan.
     * @return Value of property mFitnessParabolaSpan.
     */
    public static double getFitnessParabolaSpan() {
        return mFitnessParabolaSpan;
    }

    /*
     * Setter for property mFitnessParabolaSpan.
     * @param fitnessParabolaSpan New value of property mFitnessParabolaSpan.
     */
    public static void setFitnessParabolaSpan( double fitnessParabolaSpan ) {
        mFitnessParabolaSpan = fitnessParabolaSpan;
    }

    /*
     * Getter for property mDeathRate.
     * @return Value of property mDeathRate.
     */
    public static double getDeathRate() {
        return mDeathRate;
    }

    /*
     * Setter for property mDeathRate.
     * @param deathRate New value of property mDeathRate.
     */
    public static void setDeathRate( double deathRate ) {
        mDeathRate = deathRate;
    }

    /*
     * Getter for property mIsFixedImpact.
     * @return Value of property mIsFixedImpact.
     */
    public static boolean isFixedImpact() {
        return mIsFixedImpact;
    }

    /*
     * Setter for property mIsFixedImpact.
     * @param fixedImpact New value of property mIsFixedImpact.
     */
    public static void setFixedImpact( boolean isFixedImpact ) {
        mIsFixedImpact = isFixedImpact;
    }

    /*
     * Getter for property mIsFixedGenotype.
     * @return Value of property mIsFixedGenotype.
     */
    public static boolean isFixedGenotype() {
        return mIsFixedGenotype;
    }

    /*
     * Setter for property mIsFixedGenotype.
     * @param fixedGenotype New value of property mIsFixedGenotype.
     */
    public static void setFixedGenotype( boolean isFixedGenotype ) {
        mIsFixedGenotype = isFixedGenotype;
    }
}
