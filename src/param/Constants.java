/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Constants.java
 *
 * <p>This file holds static constants that are used throughout the system.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package param;

public class Constants {

    public static final String cVersionNumber = "1.0";
    public static final String cParameterNamesValueDelimiter = " = ";
    public static final String cParameterNameValueSpacer = ".";

    public static final String cOutputDir = "output/";

    public static final int cMinimumParameterNamesSpacing = 3;
    public final static int cImageSize = 400;

    public enum eVariableFileNamesEnum {
        POPULATIONSIZE,
        TEMPERATUREFORCING,
        TEMPERATURELOCAL,
        MEANADAPTATION,
        MEANEFFECT,
        MEANFITNESS,
        EINCREASINGCOUNT,
        EDECREASINGCOUNT
    }

    public static final String[] cVariableFileNames = {
        "PopulationSize",
        "TempForcing",
        "TempLocal",
        "MeanAdaptation",
        "MeanEffect",
        "MeanFitness",
        "EIncCount",
        "EDecCount", };
}
