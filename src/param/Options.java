/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Options.java
 *
 * <p>This class holds the static settings configured from the options dialog.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package param; // Part of the variables package

import java.util.Random; // Uses random numbers

public class Options {

    private static int mGridSize; // Size of the CA grid
    private static int mPixelSize; // Scaled size of the pixels
    private static int mSamplingRate; // Number of timesteps before collecting data
    private static String mLeftGridData; // String representation of left CA grid data
    private static String mLeftGridColour; // String representation of left CA grid colour
    private static String mRightGridData; // String representation of right CA grid data
    private static String mRightGridColour; // String representation of right CA grid colour
    private static boolean mCollectPopulationData;

    private static Random mRandom; // Global random object

    /*
     * Getter for property mCollectPopulationData.
     * @return Value of property mCollectPopulationData.
     */
    public static boolean isCollectPopulationData() {
        return mCollectPopulationData;
    }

    /*
     * Setter for property mCollectPopulationData.
     * @param collectPopulationData New value of property mCollectPopulationData.
     */
    public static void setCollectPopulationData( boolean collectPopulationData ) {
        mCollectPopulationData = collectPopulationData;
    }

    /*
     * Getter for property mRandom.
     * @return Value of property mRandom.
     */
    public static Random getRandom() {
        return mRandom;
    }

    /*
     * Setter for property mRandom.
     * @param rand New value of property mRandom.
     */
    public static void setRandom( Random random ) {
        mRandom = random;
    }

    /*
     * Getter for property mSamplingRate.
     * @return Value of property mSamplingRate.
     */
    public static int getSamplingRate() {
        return mSamplingRate;
    }

    /*
     * Setter for property mSamplingRate.
     * @param samplingRate New value of property mSamplingRate.
     */
    public static void setSamplingRate( int samplingRate ) {
        mSamplingRate = samplingRate;
    }

    /*
     * Getter for property mGridSize.
     * @return Value of property mGridSize.
     */
    public static int getGridSize() {
        return mGridSize;
    }

    /*
     * Setter for property mGridSize.
     * @param gridSize New value of property mGridSize.
     */
    public static void setGridSize( int gridSize ) {
        mGridSize = gridSize;
        resetPixelSize();
    }

    /*
     * Getter for property mPixelSize.
     * @return Value of property mPixelSize.
     */
    public static int getPixelSize() {
        return mPixelSize;
    }

    /*
     * Setter for property mPixelSize.
     */
    public static void resetPixelSize() {
        mPixelSize = Constants.cImageSize / mGridSize;
    }

    /*
     * Getter for property mLeftGridData.
     * @return Value of property mLeftGridData.
     */
    public static String getLeftGridData() {
        return mLeftGridData;
    }

    /*
     * Setter for property mLeftGridData.
     * @param leftData New value of property mLeftGridData.
     */
    public static void setLeftGridData( String leftData ) {
        mLeftGridData = leftData;
    }

    /*
     * Getter for property mLeftGridColour.
     * @return Value of property mLeftGridColour.
     */
    public static String getLeftGridColour() {
        return mLeftGridColour;
    }

    /*
     * Setter for property mLeftGridColour.
     * @param leftColour New value of property mLeftGridColour.
     */
    public static void setLeftGridColour( String leftColour ) {
        mLeftGridColour = leftColour;
    }

    /*
     * Getter for property mRightGridData.
     * @return Value of property mRightGridData.
     */
    public static String getRightGridData() {
        return mRightGridData;
    }

    /*
     * Setter for property mRightGridData.
     * @param rightData New value of property mRightGridData.
     */
    public static void setRightGridData( String rightData ) {
        mRightGridData = rightData;
    }

    /*
     * Getter for property mRightGridColour.
     * @return Value of property mRightGridColour.
     */
    public static String getRightGridColour() {
        return mRightGridColour;
    }

    /*
     * Setter for property mRightGridColour.
     * @param rightColour New value of property mRightGridColour.
     */
    public static void setRightGridColour( String rightColour ) {
        mRightGridColour = rightColour;
    }
}
