/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Tools.java
 * 
 * <p>This class is is a container class for mathematical  
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package tools;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import param.Options;

public class Tools {

    /*
     * Round with probability function to produce unbiased rounding of doubles
     * to integers. This function is a direct reimplementation of that used by
     * McDonald-Gibson (2006).
     * @param input The double input to be rounded
     * @return The rounded input
     */
    public static int roundWithProbability( double input ) {

        int floor = ( int )Math.floor( input );

        if( Options.getRandom().nextDouble() < ( input - floor ) ) {
            return floor + 1;
        } else {
            return floor;
        }
    }

    public static void centreWindow( Window frame ) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = ( int )( ( dimension.getWidth() - frame.getWidth() ) / 2 );
        int y = ( int )( ( dimension.getHeight() - frame.getHeight() ) / 2 );
        frame.setLocation( x, y );
    }

    private static double[] extendArraySize( double[] array ) {
        double[] temp = array.clone();
        array = new double[ array.length + 1 ];
        System.arraycopy( temp, 0, array, 0, temp.length );
        return array;
    }
}
