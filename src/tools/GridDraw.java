/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GridDraw.java
 *
 * <p>This class takes care of all the methods required for drawing the CA
 * images on the required graphics objects.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package tools; // Part of the drawing package

import java.awt.Color; // For creating and defining colours
import java.awt.Graphics; // For drawing
import java.awt.image.BufferedImage; // For offscreen images
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import param.Options;

public class GridDraw {

    private final BufferedImage mRightOffscreenImage; // Buffered off screen image to the right panel
    private final BufferedImage mLeftOffscreenImage; // Buffered off screen image to the left panel

    private final Graphics mRightOffscreenImageGraphics; // Graphics component of the right CA off screen image
    private final Graphics mLeftOffscreenImageGraphics; // Graphics component of the left CA off screen image
    private final Graphics mLeftGraphics;
    private final Graphics mRightGraphics;

    private final JPanel mLeftPanelGrid;
    private final JPanel mRightPanelGrid;

    private Color mColour = null; // A pointer to the colour object

    /*
     * Class constructor
     */
    public GridDraw( Graphics leftGraphics, Graphics rightGraphics, JPanel leftPanelGrid, JPanel rightPanelGrid ) {
        mLeftGraphics = leftGraphics;
        mRightGraphics = rightGraphics;
        mLeftPanelGrid = leftPanelGrid;
        mRightPanelGrid = rightPanelGrid;

        mRightOffscreenImage = new BufferedImage( 402, 402, BufferedImage.TYPE_INT_RGB );
        mLeftOffscreenImage = new BufferedImage( 402, 402, BufferedImage.TYPE_INT_RGB );

        mRightOffscreenImageGraphics = mRightOffscreenImage.getGraphics();
        mLeftOffscreenImageGraphics = mLeftOffscreenImage.getGraphics();
    }

    /*
     * Main drawing method
     * @param side The string controlling which CA grid to draw
     * @param value The value to draw as a colour
     * @param min The minimum value to display
     * @param max The maximum value to display
     * @param colour The chosen colour to draw with
     * @param i The horizontal index of the CA grid to draw
     * @param j The vertical index of the CA grid to draw
     */
    public void drawGrid( String side, double value, int min, int max, String colour, int i, int j ) {

        Graphics g = null;

        if( side.compareTo( "left" ) == 0 ) {
            g = mLeftOffscreenImageGraphics;
        } else if( side.compareTo( "right" ) == 0 ) {
            g = mRightOffscreenImageGraphics;
        }
        if( g != null ) {
            if( colour.compareTo( "jet" ) == 0 ) {
                mColour = ValueToColour.toJet( value, min, max );
            } else if( colour.compareTo( "plasma" ) == 0 ) {
                mColour = ValueToColour.toPlasma( value, min, max );
            } else if( colour.compareTo( "lava" ) == 0 ) {
                mColour = ValueToColour.toLava( value, min, max );
            } else if( colour.compareTo( "greenscale" ) == 0 ) {
                mColour = ValueToColour.toGreenscale( value, min, max );
            } else if( colour.compareTo( "greyscale" ) == 0 ) {
                mColour = ValueToColour.toGreyscale( value, min, max );
            }
            g.setColor( mColour );
            g.fillRect( ( i * Options.getPixelSize() ) + 1, ( j * Options.getPixelSize() ) + 1, ( i * Options.getPixelSize() ) + ( Options.getPixelSize() + 1 ), ( j * Options.getPixelSize() ) + ( Options.getPixelSize() + 1 ) );
        }
    }

    /*
     * Refresh grid panels on main interface
     */
    public void repaintGridPanels() {
        SwingUtilities.invokeLater( new Runnable() {

            @Override
            public void run() {
                // Repaint CA panels
                mLeftPanelGrid.repaint();
                mRightPanelGrid.repaint();
            }
        } );
    }

    /*
     * Draws the offscreen images onto the graphics passed as parameters
     */
    public void drawImages() {
        mLeftGraphics.drawImage( mLeftOffscreenImage, 0, 0, null );
        mRightGraphics.drawImage( mRightOffscreenImage, 0, 0, null );
    }
}
