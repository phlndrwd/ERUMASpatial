/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Genotype.java
 *
 * <p>This class represents the genotype of the organism, and includes a method
 * for reproduction of the gene which is subject to potential mutation.
 *
 * The code in this class is based on DoubleGenotype.java written by
 * McDonald-Gibson (2006).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model; // Part of the core model package

import param.Options;
import param.Parameters;

public class Genotype {

    private final double[] mGene; // The genotype

    /*
     * Default class constructor
     * @param var The global variables object
     */
    public Genotype() {
        mGene = new double[ 2 ];

        for( int i = 0; i < 2; i++ ) {
            mGene[ i ] = Options.getRandom().nextDouble(); // Initialise randomly
        }
    }

    /*
     * Class constructor with optional genetic information passed as a parameter.
     * @param gene The passed genetic information
     */
    public Genotype( double[] gene ) {
        mGene = gene;
    }

    /*
     * Method to reproduce this gene, apply any relevant mutation and return
     * either the mutated or the copied gene.
     * @return The returned genotype
     */
    public Genotype reproduce() {

        double[] childGene = new double[ 2 ];  // new child

        // For each loci
        for( int i = 0; i < 2; i++ ) {
            // If there will be mutation
            if( !Parameters.isFixedGenotype() && ( Options.getRandom().nextDouble() < Parameters.getMutationRate( i ) ) ) {
                // Mutate
                childGene[ i ] = mGene[ i ] + Options.getRandom().nextGaussian() * Parameters.getMutationStandDev( i );
                // Perform reflexion maintaining alleles between 0 and 1.
                if( childGene[ i ] < 0 ) {
                    childGene[ i ] = 0 - childGene[ i ];
                } else if( childGene[ i ] > 1 ) {
                    childGene[ i ] = 2 - childGene[ i ];
                }
            } else {
                // Else, return an exact clone of this genotype
                childGene[ i ] = mGene[ i ];
            }
        }

        return new Genotype( childGene );
    }

    /*
     * Getter for this genotype
     * @return The genotype
     */
    public double[] getGene() {
        return mGene;
    }
}
