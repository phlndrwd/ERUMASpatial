/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainThread.java
 *
 * <p>This class is the main application thread that creates the global
 * environment and orchestrates the various operations on it from within the 
 * main loop of time.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model; // Part of the core model package

import tools.ValueToColour; // Translates numerical values to colours
import java.awt.Color; // Handles colours
import javax.swing.JFrame; // To maximize parent window
import ui.MainInterface; // Handles reference to the main UI
import param.Options;
import param.Parameters; // Uses model variables

public class MainThread implements Runnable {

    private final Environment mEnvironment;
    private final MainInterface mParent; // A reference to the parent

    /*
     * Class constructor, initializes objects, takes a local pointer to the 
     * variables object and sets up the environment.
     * @param parent The reference to the main UI class
     */
    public MainThread( MainInterface parent ) {

        mParent = parent;
        mEnvironment = new Environment( mParent );
    }

    /*
     * Make method calls to the progress meter during the main time loop
     */
    private void updateProgressBar( int t ) {

        Color c = ValueToColour.toJet( t, 0, Parameters.getTimeMaximum() );
        mParent.mProgressBar.setForeground( c );
        mParent.mProgressBar.setValue( t );
        mParent.mProgressBar.setString( "t = " + t );
    }

    /*
     * Implementation of the run method from the runnable interface, the method
     * that sets the thread running.
     */
    @Override
    public void run() {

        boolean isRunComplete = false;

        for( int t = 0; t <= Parameters.getTimeMaximum(); t++ ) {

            mEnvironment.applyNeighbourEffects();
            mEnvironment.applyLocalEffects( t );
            mEnvironment.updatePopulations();

            if( t % Options.getSamplingRate() == 0 ) {

                mEnvironment.updateOutput( t );
                updateProgressBar( t );
            }
            // If at the end of the simulation
            if( t == Parameters.getTimeMaximum() ) {
                isRunComplete = true;
            }
            // If the thread has been told to stop, end the loop
            if( !mParent.isThreadRunning() ) {
                break;
            }
        } // End t
        // End terminate

        // If the model run was completed, write data to file
        if( isRunComplete ) {
            mEnvironment.endOutput();
            mParent.setState( JFrame.NORMAL );
            mParent.toFront();
        }
        // Stop this thread once everything has been completed
        mParent.stopThread();
    }
}
