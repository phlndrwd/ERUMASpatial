/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Environment.java
 *
 * <p>This class represents the global environment. It holds the two-dimensional
 * array of populations, carries out the appropriate method of temperature
 * forcing on them. It also makes the relevent class method calls to collect the
 * statistics produced by each population and produce real-time graphical
 * output.
 *
 * The code in this class is based on Environment.java written by
 * McDonald-Gibson (2006).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model; // Part of the core model package

import data.DataRecorder; // Records data
import ui.MainInterface; // For reference to parent
import tools.Tools; // For drawing the grid
import java.util.ArrayList; // Handles neighbour populations and variable names
import java.util.List; // Local pointer to neighbour populations
import param.Options; // Uses model options
import param.Parameters; // Uses model parameters

public class Environment {

    private final Population[][] mPopulations; // Two-dimensional array of populations
    private final DataRecorder mDataRecorder; // Data recording object

    List<Population> mNeighbours; // Global neighbours object to reduce garbage collection

    /*
     * Class constructor
     * @param parent A reference to the MainInterface.
     */
    public Environment( MainInterface parent ) {

        mDataRecorder = new DataRecorder( parent );

        mPopulations = new Population[ Options.getGridSize() ][ Options.getGridSize() ];

        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {

                if( Parameters.isFixedTemperature() ) {
                    mPopulations[ i ][ j ] = new Population( Parameters.getTemperatureMinimum() );
                } else {
                    mPopulations[ i ][ j ] = new Population( Math.round( ( float )latitudeDependentTemperature( 0, j ) ) );
                }
            } // End j
        } // End i
    }

    /*
     * This function returns latitude dependent temperature forcing according to
     * an inverse parabolic function when given a timestep and a latitude for
     * the population to receive it, see equation (11).
     * @param t The timestep
     * @param latitude The latitude
     * @return The appropriate temperature forcing
     */
    private double latitudeDependentTemperature( int t, int latitude ) {

        double temperature = 4 * Parameters.getTemperatureVariation() * ( -Math.pow( ( double )latitude / ( double )( Options.getGridSize() - 1 ) - 0.5, 2 ) + 0.25 ) + t * Parameters.getTemperatureIncrement() + Parameters.getTemperatureMinimum();

        return temperature;
    }

    /*
     * Step through the populations, apply the relevant temperature forcing and
     * update the each with the effects of the organisms.
     * @param t The current time-step
     */
    public void applyLocalEffects( int t ) {

        double tempForcing;

        // Calculate local effects
        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {

                if( Parameters.isFixedTemperature() ) {
                    tempForcing = t * Parameters.getTemperatureIncrement() + Parameters.getTemperatureMinimum();
                } else {
                    tempForcing = latitudeDependentTemperature( t, j );
                }

                mPopulations[ i ][ j ].update( tempForcing );
            } // End j
        } // End i
    }

    /*
     * Step through the populations and calculate the effects of the neighbours
     * as a result of temperature diffusion between hotter/colder areas. The
     * result is calculated and added to a temporary variable in each population
     * so that this is calculated and applied simultaneously.
     */
    public void applyNeighbourEffects() {
        // Calculate diffusion/leakage between neighbouring populations
        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {

                //List<Population> neighbours = getNeighbours( i, j );
                //mNeighbours.clear();
                getNeighbours( i, j );

                double neighbourImpact = 0;

                for( Population neighbourPopulation: mNeighbours ) {
                    neighbourImpact += ( ( 1 - Parameters.getDiffusionParameter() ) * ( neighbourPopulation.getTempLocal() - mPopulations[ i ][ j ].getTempLocal() ) / 2 );
                } // End iterator

                mPopulations[ i ][ j ].addNeighbourImpact( neighbourImpact );
            } // End j
        } // End i

        // Apply diffusion/leakage between neighbouring populations
        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {

                mPopulations[ i ][ j ].commitNeighbourImpact();

            } // End j
        } // End i
    }

    /*
     * A function to return a list of neighbouring populations given horizontal
     * and vertical coordinates of the source population.
     * @param x The horizontal coordinate in the array of populations
     * @param y The vertical coordinate in the array of populations
     * @return The list of neighbours
     */
    private List<Population> getNeighbours( int x, int y ) {

        //List<Population> neighbours = new ArrayList<>();
        mNeighbours = new ArrayList<>();

        for( int i = ( x - 1 ); i <= ( x + 1 ); i++ ) {
            for( int j = ( y - 1 ); j <= ( y + 1 ); j++ ) {

                if( ( i != x ) || ( j != y ) ) {
                    if( ( i >= 0 ) && ( i < Options.getGridSize() ) && ( j >= 0 ) && ( j < Options.getGridSize() ) ) {

                        mNeighbours.add( mPopulations[ i ][ j ] );
                    }
                }
            } // End j/home/philju/Dropbox/Development/Repositories/ERUMASpatial/src/model/Environment.java:154
        } // End i

        return mNeighbours;
    }

    public void updatePopulations() {

        for( int i = 0; i < Options.getGridSize(); i++ ) {
            for( int j = 0; j < Options.getGridSize(); j++ ) {

                // Update the local population
                if( mPopulations[ i ][ j ].getPopulationSize() > 0 ) {
                    // Calculate number of organisms to die by rounding up
                    int numToDie = Tools.roundWithProbability( mPopulations[ i ][ j ].getPopulationSize() * Parameters.getDeathRate() );

                    // Run tournaments over the population for number of deaths
                    for( int k = 0; k < numToDie; k++ ) {

                        mPopulations[ i ][ j ].tournament();
                    }
                }
                // PJU FIX - Seeding and migration algorithms can result in populations over 250 in size.
                seedPopulations( i, j );
                // If migration should occur
                if( Parameters.getMigrationScheme() != 5 ) {
                    performMigration( i, j );
                } // End if migration
            } // End j
        } // End i
        // If migration has occured
        if( Parameters.getMigrationScheme() != 5 ) {
            // Perform immigration simultaneously
            for( int i = 0; i < Options.getGridSize(); i++ ) {
                for( int j = 0; j < Options.getGridSize(); j++ ) {
                    mPopulations[ i ][ j ].performImmigration();
                } // End j
            } // End i
        }
    }

    private void performMigration( int i, int j ) {
        // Perform migration on this population
        int numToMigrate = Tools.roundWithProbability( mPopulations[ i ][ j ].getPopulationSize() * Parameters.getMigrationProbability() );

        getNeighbours( i, j );

        for( int k = 0; k < numToMigrate; k++ ) {

            if( mNeighbours.size() > 0 ) { // for one population only

                Organism migrant = mPopulations[ i ][ j ].getMigrant();
                double[] score = new double[ mNeighbours.size() ];

                // Migration using population or combined scoring
                if( ( Parameters.getMigrationScheme() == 1 )
                        || ( Parameters.getMigrationScheme() == 3 ) ) {

                    double popDensity;
                    for( int l = 0; l < mNeighbours.size(); l++ ) {

                        Population neighbourPopulation = mNeighbours.get( l );
                        popDensity = 1 - ( neighbourPopulation.getPopulationSize() / Parameters.getCarryingCapacity() );
                        score[ l ] += popDensity;
                    }
                }
                // Migration using fitness or combined scoring
                if( ( Parameters.getMigrationScheme() == 2 )
                        || ( Parameters.getMigrationScheme() == 3 ) ) {
                    for( int l = 0; l < mNeighbours.size(); l++ ) {
                        Population neighbourPopulation = mNeighbours.get( l );
                        migrant.updateFitness( neighbourPopulation.getTempLocal() );
                        score[ l ] += migrant.getFitness();
                    }
                }

                double highScore = 0;
                int highIndex = -1;
                // Fitness using scoring of any type
                if( Parameters.getMigrationScheme() != 4 ) {
                    // Select the index of the highest scoring neighbour
                    for( int l = 0; l < mNeighbours.size(); l++ ) {
                        if( score[ l ] > highScore ) {
                            highScore = score[ l ];
                            highIndex = l;
                        }
                    }
                }
                // Place the migrant in the winner if one was found
                if( highIndex != -1 ) {
                    Population neighbourPopulation = mNeighbours.get( highIndex );
                    neighbourPopulation.addImmigrant( migrant );
                } else { // Else, pick a neighbour at random
                    highIndex = Options.getRandom().nextInt( mNeighbours.size() );
                    Population randomPopulation = mNeighbours.get( highIndex );
                    randomPopulation.addImmigrant( migrant );
                }
            } // End neighbours > 0
        } // End migration for this population
    }

    private void seedPopulations( int i, int j ) {

        // If fixed population, force maximum number of individuals
        if( Parameters.isFixedPopulation() ) {

            while( mPopulations[ i ][ j ].getPopulationSize() < Parameters.getCarryingCapacity() ) {

                // Force the population to maximum capacity
                mPopulations[ i ][ j ].addOrganism( new Organism() );
            }
        } else // Else, using a variable population
        {
            if( mPopulations[ i ][ j ].getPopulationSize() < Parameters.getCarryingCapacity() ) {
                // Seed population with new individuals
                int numToSeed = Tools.roundWithProbability( ( 1 - ( ( double )mPopulations[ i ][ j ].getPopulationSize() / ( double )Parameters.getCarryingCapacity() ) ) * Parameters.getSeedRate() );

                for( int k = 0; k < numToSeed; k++ ) {
                    mPopulations[ i ][ j ].addOrganism( new Organism() );
                }
            }
        }
    }

    /*
     * Collects statistics for each population, adds them to the relevant data
     * logging objects and makes method calls to draw real-time graphical output
     * if required. Make a safe call back to the UI to repaint the grid panels
     * when drawn.
     * @param t The current time step.
     */
    public void updateOutput( int t ) {

        mDataRecorder.updateOutput( mPopulations, t );
    }

    /*
     * Write the collected data to file
     */
    public void endOutput() {
        mDataRecorder.endOutput();
    }
}
