/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Organism.java
 *
 * <p>This class represents an individual organism and so holds the genetic
 * information and the decoded result of that genotype when converted to a
 * phenotype.
 *
 * The code in this class is based on Individual.java written by
 * McDonald-Gibson (2006).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import param.Parameters;

public final class Organism {

    private final Genotype mGenotype; // The genotype

    private boolean mIsEIncreasing; // Whether or not the organism increases temperature
    private double mFitness; // The fitness of this individual
    private double mEffect; // The effect this individual has on the temperature
    private double mAdaptation; // This individual's optimum temperature

    /*
     * Default class constructor
     */
    public Organism() {

        mGenotype = new Genotype();
        genotypeToPhenotype();
        mFitness = 0;
    }

    /*
     * Class constructor with option to pass the genotype as a parameter.
     * @param genotype The passed genotype
     */
    public Organism( Genotype genotype ) {

        mGenotype = genotype;
        genotypeToPhenotype();
        mFitness = 0;
    }

    /*
     * Convert the genotype into the phenotype, thus determining the relationship
     * this organism has with the environment.
     */
    public void genotypeToPhenotype() {

        double[] gene = mGenotype.getGene();

        // Find the point of adaptation within the habitable range
        mAdaptation = gene[ 0 ] * ( Parameters.getAdaptionMaximum() - Parameters.getAdaptionMinimum() ) + Parameters.getAdaptionMinimum();

        if( Parameters.isFixedImpact() ) { // If using fixed impact

            if( gene[ 1 ] >= 0.5 ) { // Alter effect and set allele type                
                mEffect = 1.0;
                mIsEIncreasing = true;
            } else if( gene[ 1 ] < 0.5 ) {
                mEffect = -1.0;
                mIsEIncreasing = false;
            }
        } else { // Else, calculate effect and set allele type
            mEffect = ( 2 * gene[ 1 ] ) - 1; // Find effect if using continous impact

            if( gene[ 1 ] >= 0.5 ) {
                mIsEIncreasing = true;
            } else if( gene[ 1 ] < 0.5 ) {
                mIsEIncreasing = false;
            }
        }
    }

    /*
     * Update the fitness according to equation (1) McDonald-Gibson et al. (2008)
     */
    public void updateFitness( double temperature ) {
        mFitness = Math.max( 0, 1 - Math.pow( ( mAdaptation - temperature ) / Parameters.getFitnessParabolaSpan(), 2 ) );
    }

    /*
     * Get a child of this individual
     * @return The child organism
     */
    public Organism getChild() {
        return new Organism( mGenotype.reproduce() );
    }

    /*
     * Getter for property mFitness.
     * @return Value of property mFitness.
     */
    public double getFitness() {
        return mFitness;
    }

    /*
     * Getter for property mEffect.
     * @return Value of property mEffect.
     */
    public double getEffect() {
        return mEffect;
    }

    /*
     * Getter for property mAdaptation.
     * @return Value of property mAdaptation.
     */
    public double getAdaptation() {
        return mAdaptation;
    }

    /*
     * Getter for property mIsEIncreasing.
     * @return Value of property mIsEIncreasing.
     */
    public boolean isEIncreasing() {
        return mIsEIncreasing;
    }
}
