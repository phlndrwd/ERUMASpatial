/*
 *   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Population.java
 *
 * <p>This class represents a population of organisms. It holds the relevant
 * list data structures of population members, migrants and statistics of this
 * population including the temperature, and relevant methods to interact with
 * and update these variables accordingly.
 *
 * The code in this class is based on Population.java written by
 * McDonald-Gibson (2006).
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model; // Part of the core model package

import data.DataWriter;
import java.util.ArrayList; // Handles lists of organisms
import param.Options;
import param.Parameters; // Uses model variables
import tools.Tools;

public class Population {

    private final ArrayList<Organism> mOrganisms; // The list of organisms
    private final ArrayList<Organism> mMigrants; // The list of immigrants

    Organism mA;
    Organism mB;
    Organism mWinner;
    Organism mLoser;

    private double mTempLocal; // The local temperature of this population
    private double mTempForcing; // Temperature this population is forced towards
    private double mNeighbourImpact; // Impact on local temp. due to diffusion
    private double mMeanAdaptation; // Average level of adaptation
    private double mMeanFitness; // Average fitness of the population
    private double mMeanEffect; // Average impact on the resource
    private int mEIncreasingCount; // Number of E alleles
    private int mEDecreasingCount; // Number of e alleles

    /*
     * Class constructor, initialized to a starting temperature according to the
     * appropriate forcing this population will receive.
     * @param var The global variables object
     * @param tempInit The temperature to initialize this population to
     */
    public Population( int tempInit ) {

        mTempLocal = tempInit;
        mOrganisms = new ArrayList<>();
        mMigrants = new ArrayList<>();
    }

    /*
     * Calculate the effect this population has on the local temperature
     * @param tempForcing Temperature towards which this population is forced
     */
    public void update( double tempForcing ) {

        mTempForcing = tempForcing;
        double populationImpact = 0;     // The impact of the whole population
        // Iterate through the population
        for( Organism organism: mOrganisms ) {
            populationImpact += organism.getEffect() * Parameters.getNicheConstructionStrength();
        }
        // Update the local temperature by this amount
        updateLocalTemp( populationImpact );
    }

    /*
     * The functionality of the steady-state GA is contained within this method.
     * @param population The population over which to apply the tournament
     */
    public void tournament() {

        //Organism a; // The first random member of the population
        //Organism b; // The second random member of the population
        //Organism winner; // The winner of the tournament
        //Organism loser; // The loser of the tournament
        // Retrieve random members of the population
        mA = getRandomOrganism();
        mB = getRandomOrganism();

        // Determine a winner by comparing fitnesses
        if( mA.getFitness() >= mB.getFitness() ) {
            mWinner = mA;
            mLoser = mB;
        } else {
            mWinner = mB;
            mLoser = mA;
        }

        if( Parameters.isFixedPopulation() ) {

            // Remove both if their fitnesses are at zero
            if( ( mWinner.getFitness() == 0 ) && ( mLoser.getFitness() == 0 ) ) {

                removeOrganism( mLoser );
                removeOrganism( mWinner );

            } else { // Else, remove the loser and add a child from the winner

                removeOrganism( mLoser );
                addOrganism( mWinner.getChild() );
            }
        } else {
            /*
             * Ignore cases of equal fitness, remove the loser and produce
             * children of the winner
             */
            removeOrganism( mLoser );

            if( Options.getRandom().nextDouble() < mWinner.getFitness() ) {
                /*
                 * Calculate number of winner's offspring, according to equation
                 * (2) of McDonald-Gibson et al. (2008).
                 */
                int numOffspring = Tools.roundWithProbability( mWinner.getFitness() * Math.exp( 1 - ( ( double )getPopulationSize() / ( double )Parameters.getCarryingCapacity() ) ) );
                // Produce the required amount of children
                for( int i = 0; i < numOffspring; i++ ) {

                    addOrganism( mWinner.getChild() );
                }
            }
        }
    }

    // Method to alter the local temperature with the population effects
    private void updateLocalTemp( double amount ) {

        mTempLocal += Parameters.getForcingStrength() * ( mTempForcing - mTempLocal );
        changeLocalTemp( amount );
    }

    /*
     * Add the impact the neighbouring populations to a temporary variable for
     * updating simultaneously.
     */
    public void addNeighbourImpact( double neighbourImpact ) {
        mNeighbourImpact = neighbourImpact;
    }

    /*
     * Apply the effect of the neighbours
     */
    public void commitNeighbourImpact() {
        changeLocalTemp( mNeighbourImpact );
    }

    /*
     * Add the amount passed as a parameter to the local temperature to apply
     * its effect.
     * @param amount The amount by which to change the local temperature
     */
    public void changeLocalTemp( double amount ) {

        mTempLocal += amount;

        if( Parameters.isFixedTemperature() ) {
            if( mTempLocal > Parameters.getTemperatureMaximum() ) {
                mTempLocal = ( Parameters.getTemperatureMaximum() );
            }
        } else if( mTempLocal > ( Parameters.getTemperatureMaximum() + Parameters.getTemperatureVariation() ) ) {
            mTempLocal = ( Parameters.getTemperatureMaximum() + Parameters.getTemperatureVariation() );
        }
        if( mTempLocal < Parameters.getTemperatureMinimum() ) {
            mTempLocal = Parameters.getTemperatureMinimum();
        }
    }

    /*
     * Update the fitnesses of the organisms in this population
     */
    public void updateFitnesses() {

        for( Organism organism: mOrganisms ) {
            organism.updateFitness( mTempLocal );
        }
    }

    /*
     * Retrieve a random organism from this population
     * @return The random organism
     */
    public Organism getRandomOrganism() {

        Organism organism = mOrganisms.get( Options.getRandom().nextInt( getPopulationSize() ) );
        organism.updateFitness( mTempLocal );

        return organism;
    }

    /*
     * Remove a migrant from this population by retrieving randomly and making
     * it leave this population.
     * @return The migrant
     */
    public Organism getMigrant() {

        Organism organism = getRandomOrganism();
        mOrganisms.remove( organism );

        return organism;
    }

    /*
     * Calculate the statistics of this population
     */
    public void calcMeanData( DataWriter dataLogger, int populationIndex, int sampleIndex ) {

        double adaption;
        double totalEffect = 0;         // The total effect of the population
        double totalFitness = 0;        // The total fitness of the population
        double totalAdaptation = 0;     // The total adaptation of the population
        mEIncreasingCount = 0;                  // Initialise E allele counter
        mEDecreasingCount = 0;                  // Initialise e allele counter

        // Iterate through the population in order to calculate totals
        for( int i = 0; i < getPopulationSize(); i++ ) {

            Organism organism = mOrganisms.get( i );
            adaption = organism.getAdaptation();

            dataLogger.addPopulationData( populationIndex, i, sampleIndex, adaption );

            totalEffect += organism.getEffect();
            totalFitness += organism.getFitness();
            totalAdaptation += adaption;

            // Count the number of E/e alleles
            if( organism.isEIncreasing() ) {
                mEIncreasingCount += 1;
            } else {
                mEDecreasingCount += 1;
            }
        }

        // Calculate averages by dividing totals by the population size
        mMeanAdaptation = totalAdaptation / getPopulationSize();
        mMeanFitness = totalFitness / getPopulationSize();
        mMeanEffect = totalEffect / getPopulationSize();
    }

    /*
     * Add an organism to this population
     */
    public void addOrganism( Organism organism ) {
        mOrganisms.add( organism );
    }

    /*
     * Remove an organism from this population
     */
    public void removeOrganism( Organism organism ) {
        mOrganisms.remove( organism );
    }

    /*
     * Add an immigrant to the temporary list
     */
    public void addImmigrant( Organism immigrant ) {
        mMigrants.add( immigrant );
    }

    /*
     * Perform immigration on this population
     */
    public void performImmigration() {

        for( Organism migrant: mMigrants ) {
            // If the population is below carrying capacity, add migrant
            if( getPopulationSize() < Parameters.getCarryingCapacity() ) {

                addOrganism( migrant );
            } else {
                /*
                * Else, perform competition between migrant and random
                * population member
                 */
                Organism organism = getRandomOrganism();
                /*
                * Migrant only succeeds if its fitness is greater than the
                * challenger
                 */
                if( migrant.getFitness() > organism.getFitness() ) {

                    removeOrganism( organism );
                    addOrganism( migrant );
                }
            }
        } // End iterator

        mMigrants.clear();
    }

    /*
     * Gets the size of this population
     */
    public int getPopulationSize() {
        return mOrganisms.size();
    }

    /*
     * Getter for property mTempLocal.
     * @return Value of property mTempLocal.
     */
    public double getTempLocal() {
        return mTempLocal;
    }

    /*
     * Getter for property mMeanAdaptation.
     * @return Value of property mMeanAdaptation.
     */
    public double getMeanAdaptation() {
        return mMeanAdaptation;
    }

    /*
     * Getter for property mMeanFitness.
     * @return Value of property mMeanFitness.
     */
    public double getMeanFitness() {
        return mMeanFitness;
    }

    /*
     * Getter for property mMeanEffect.
     * @return Value of property mMeanEffect.
     */
    public double getMeanEffect() {
        return mMeanEffect;
    }

    /*
     * Getter for property mEIncreasingCount.
     * @return Value of property mEIncreasingCount.
     */
    public int getEIncreasingCount() {
        return mEIncreasingCount;
    }

    /*
     * Getter for property mEDecreasingCount.
     * @return Value of property mEDecreasingCount.
     */
    public int getEDecreasingCount() {
        return mEDecreasingCount;
    }

    /*
     * Getter for property mTempForcing.
     * @return Value of property mTempForcing.
     */
    public double getTempForcing() {
        return mTempForcing;
    }
}
