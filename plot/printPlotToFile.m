function printPlotToFile( plotHandle, filePath, fileFormat )

imageWidth = 10;
imageHeight = 8;

set( plotHandle,'PaperUnits','centimeters', 'PaperSize', [ imageWidth imageHeight ], 'PaperPosition', [ 0 0 imageWidth imageHeight ] );

if strcmpi( fileFormat, 'eps' ) == 1
    print( plotHandle, [ filePath '.' lower( fileFormat ) ], '-depsc', '-loose'  );
elseif strcmpi( fileFormat, 'png' ) == 1
    print( plotHandle, [ filePath '.' lower( fileFormat ) ], '-dpng', '-r300' );
end