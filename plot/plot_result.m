%%
 %   ERUMASpatial - A spatial extension of McDonald-Gibson et al. (2008).
 %   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 %
 %   This program is free software: you can redistribute it and/or modify
 %   it under the terms of the GNU General Public License as published by
 %   the Free Software Foundation, either version 3 of the License, or
 %   (at your option) any later version.
 %
 %   This program is distributed in the hope that it will be useful,
 %   but WITHOUT ANY WARRANTY; without even the implied warranty of
 %   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 %   GNU General Public License for more details.
 %
 %   You should have received a copy of the GNU General Public License
 %   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 %
 % plot_result.m
 %
 % This script plots the output files from ERUMASpatial using MATLAB or GNU
 % Octave (https://gnu.org/software/octave/). To use, set "relativePath" to 
 % point to the location of the "output" directory. Then set "dataSet" to
 % the particular set of results you wish to plot. Then run the script. The
 % script will generate file prints of the plots in the specified raster or
 % vector image format (PNG or EPS).
 %
 % Note that the upper and lower bounds of habitability are assumed to
 % remain unchanged from their default values of 85 and 15 respectively.
 % These will need to be manually altered to reflect custom values on the
 % plots. Also, the input file names are assumed to remain the same as their
 % default. These will need to be manually altered to reflect any relevant 
 % code changes. 
 % 
 %  author     Phil Underwood
 %  email      philjunderwood@gmail.com
%%

clear all;

%% User Defined Parameters
dataSet = '2015-07-16_16-35';
relativePath =  '../output/';
outputFileFormat = 'eps'; % EPS or PNG

upperHabitableBound = 85;
lowerHabitableBound = 15;

load( [ relativePath dataSet '/Time.csv' ] );
load( [ relativePath dataSet '/TempLocal.csv' ] );
load( [ relativePath dataSet '/TempForcing.csv' ] );
load( [ relativePath dataSet '/MeanAdaptation.csv' ] );
load( [ relativePath dataSet '/EIncCount.csv' ] );
load( [ relativePath dataSet '/EDecCount.csv' ] );

habitableBounds( 1, 1:length( Time ) ) = lowerHabitableBound; 
habitableBounds( 2, 1:length( Time ) ) = upperHabitableBound;

%% Plotting
first = figure;
plot( Time, TempForcing );
hold on;
plot( Time, TempLocal );
plot( Time, habitableBounds( 1, : ), 'g--' );
plot( Time, habitableBounds( 2, : ), 'g--' );
xlabel( 'Time' );
ylabel( 'Temperature' );
title( 'Changes in local temperature from each population over time' );
printPlotToFile( first, [ relativePath dataSet '/1_plot' ], outputFileFormat );

second = figure;
plot( Time, mean( TempForcing ), 'k--' );
hold on;
plot( Time, mean( TempLocal ), 'b-' );
plot( Time, mean( MeanAdaptation ), 'r-' );
plot( Time, habitableBounds( 1, : ), 'g--' );
plot( Time, habitableBounds( 2, : ), 'g--' );
xlabel( 'Time' );
ylabel( 'Temperature' );
title( 'Mean changes in local temperature over time' );
legend( 'Temp. Forcing', 'Temp. Local', 'Mean Adaptation', 'Habitable Bounds', 'Location', 'NorthWest' );
printPlotToFile( second, [ relativePath dataSet '/2_plot' ], outputFileFormat );

Effect( :, 1 ) = mean( EIncCount );
Effect( :, 2 ) = mean( EDecCount );

third = figure;
bar( Time, Effect, 'stack')
axis( [ 0 max( Time ) 0 round( max( sum( Effect, 2 ) ) ) ] );
xlabel( 'Time' );
ylabel( 'Frequency' );
title( 'Frequency of E alleles over time' );
legend( 'E Increasing', 'E Decreasing', 'Location', 'NorthWest' );
printPlotToFile( third, [ relativePath dataSet '/3_plot' ], outputFileFormat );


